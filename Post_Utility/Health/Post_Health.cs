﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Post_Utility.Database;

namespace Post_Utility.Health
{
    public static class Post_Health
    {
        public static DataTable GetSumInsuredPriceList()
        {
            string fileds = "*";
            string tablename = "tbl_SumInsuredMaster order by suminsuredid";
            return Post_Database.GetRecordFromTable(fileds, tablename, "", "api");
        }

        public static DataTable GetNewEnquiryHealthDetails(string enquiryid)
        {
            string fileds = "*";
            string tablename = "T_NewEnquiry_Health";
            string wherecon = "Enquiry_Id='" + enquiryid + "'";
            return Post_Database.GetRecordFromTable(fileds, tablename, wherecon, "api");
        }
        public static DataTable GetEnquiry_Response(string enquiryid)
        {
            string fileds = "*";
            string tablename = "T_Enquiry_Response";
            string wherecon = "enquiry_id='" + enquiryid + "'";
            return Post_Database.GetRecordFromTable(fileds, tablename, wherecon, "api");
        }
        public static DataTable GetNewEnquiry_Health(string enquiryid)
        {
            string fileds = "*";
            string tablename = "T_NewEnquiry_Health";
            string wherecon = "Enquiry_Id='" + enquiryid + "'";
            return Post_Database.GetRecordFromTable(fileds, tablename, wherecon, "api");
        }
        public static DataTable GetMaster_PinCode(string pincode)
        {
            string fileds = "top 1 City";
            string tablename = "tblMaster_PinCode";
            string wherecon = "Pincode='" + pincode + "'";
            return Post_Database.GetRecordFromTable(fileds, tablename, wherecon, "motor");
        }
        public static bool InsertSelectedPlanProposal(string enquiryid, string productid, string companyid, string suminsured, string periodfor, string premium, string discount, string policyfor, string plantype)
        {
            string fileds = "enquiryid,productid,companyid,suminsured,periodfor,premium,discount,policyfor,plantype";
            string fieldValue = "'" + enquiryid + "','" + productid + "','" + companyid + "','" + suminsured + "','" + periodfor + "','" + premium + "','" + discount + "','" + policyfor + "','" + plantype + "'";

            return Post_Database.InsertRecordToTable(fileds, fieldValue, "T_Health_Proposal", "api");
        }
        public static bool UpdateSelectedPlanProposal(string suminsured, string periodfor, string premium, string discount, string policyfor, string plantype, string enquiryid, string productid, string companyid)
        {
            string fieldsWithValue = "suminsured='" + suminsured + "',periodfor='" + periodfor + "',premium='" + premium + "',discount='" + discount + "',policyfor='" + policyfor + "',plantype='" + plantype + "'";
            string whereCondition = "enquiryid='" + enquiryid + "' and productid='" + productid + "' and companyid='" + companyid + "'";

            return Post_Database.UpdateRecordIntoAnyTable("T_Health_Proposal", fieldsWithValue, whereCondition, "api");
        }
        public static bool InsertProposerDetailsProposal(string dob, string address1, string address2, string landmark, int stateid, string statename, int cityid, string cityname, string firstname, string lastname, string mobile, string emailid, string pincode, string gender, bool isproposerinsured, string title, string panno, string aadharno, string annualincome, bool ispropnominee)
        {
            string fileds = "dob,address1,address2,landmark,stateid,statename,cityid,cityname,firstname,lastname,mobile,emailid,pincode,gender,isproposerinsured,title,panno,aadharno,annualincome,ispropnominee";
            string fieldValue = "'" + dob + "','" + address1 + "','" + address2 + "','" + landmark + "','" + stateid + "','" + statename + "','" + cityid + "','" + cityname + "','" + firstname + "','" + lastname + "','" + mobile + "','" + emailid + "','" + pincode + "','" + gender + "'," + isproposerinsured + ",'" + title + "','" + panno + "','" + aadharno + "','" + annualincome + "'," + ispropnominee + "";

            return Post_Database.InsertRecordToTable(fileds, fieldValue, "T_Health_Proposal", "api");
        }
        public static bool UpdateProposerDetailsProposal(string dob, string address1, string address2, string landmark, int stateid, string statename, int cityid, string cityname, string title, string firstname, string lastname, string mobile, string emailid, string pincode, string gender, int areaid, string areaname, string panno, string aadharno, string annualincome, string enquiryid, string productid, string companyid, bool criticalillness, bool isproposerinsured, bool ispropnominee)
        {
            string fieldsWithValue = "dob='" + dob + "',address1='" + address1 + "',address2='" + address2 + "',landmark='" + landmark + "',statename='" + statename + "',title='" + title + "',cityid='" + cityid + "',cityname='" + cityname + "',firstname='" + firstname + "',lastname='" + lastname + "',mobile='" + mobile + "',emailid='" + emailid + "',pincode='" + pincode + "',gender='" + gender + "',areaid='" + areaid + "',areaname='" + areaname + "',criticalillness=" + (criticalillness ? 1 : 0) + ",isproposerinsured=" + (isproposerinsured ? 1 : 0) + ",ispropnominee=" + (ispropnominee ? 1 : 0) + ",panno='" + panno + "',aadharno='" + aadharno + "',annualincome='" + annualincome + "'";
            string whereCondition = "enquiryid='" + enquiryid + "' and productid='" + productid + "' and companyid='" + companyid + "'";

            return Post_Database.UpdateRecordIntoAnyTable("T_Health_Proposal", fieldsWithValue, whereCondition, "api");
        }
        public static DataTable IsEnquiryDetailExistOrNot(string enquiryid, string productid, string companyid)
        {
            string fileds = "*";
            string tablename = "T_Health_Proposal";
            string wherecon = "enquiryid='" + enquiryid + "' and productid='" + productid + "' and companyid='" + companyid + "'";
            return Post_Database.GetRecordFromTable(fileds, tablename, wherecon, "api");
        }
        public static DataTable GetProposalDetail(string enquiryid, string productid = "", string companyid = "")
        {
            string wherecon = "enquiryid='" + enquiryid + "'";

            if (!string.IsNullOrEmpty(productid.Trim()))
            {
                wherecon = wherecon + "  and productid='" + productid + "'";
            }
            if (!string.IsNullOrEmpty(companyid.Trim()))
            {
                wherecon = wherecon + "  and companyid='" + companyid + "'";
            }

            return Post_Database.GetRecordFromTable("*", "T_Health_Proposal", wherecon, "api");
        }
        public static DataTable GetRelation(string companyid, string productId, string policyfor)
        {
            string wherecon = "CompanyId='" + companyid + "' and status=1";
            if (companyid == "3")
            {
                wherecon = wherecon + " and RelationType like '%" + policyfor + "%'";
            }

            if (companyid == "1")
            {
                wherecon = wherecon + " and productdetailid='" + productId + "'";
            }

            return Post_Database.GetRecordFromTable("*", "T_Relationship_Common", wherecon, "api");
        }
        public static bool UpdateSearchUrlPara_InEnquiry_Response(string enquiryid, string urlpara)
        {
            string[] paraSplit = urlpara.Split('&');

            string respoFieldsWithValue = "productid='" + paraSplit[1].Split('=')[1] + "',suminsuredid='" + paraSplit[3].Split('=')[1] + "',suminsured='" + paraSplit[4].Split('=')[1] + "',companyid='" + paraSplit[2].Split('=')[1] + "'";
            return Post_Database.UpdateRecordIntoAnyTable("T_Enquiry_Response", respoFieldsWithValue, "enquiry_id='" + enquiryid + "'", "api");
        }
        public static bool UpdateSearchUrlPara_InNewEnquiry_Health(string enquiryid, string urlpara, string selectedproductjson)
        {
            string fieldsWithValue = "searchurl='" + urlpara + "',selectedproductjson='" + selectedproductjson + "'";
            string whereCondition = "Enquiry_Id='" + enquiryid + "'";
            return Post_Database.UpdateRecordIntoAnyTable("T_NewEnquiry_Health", fieldsWithValue, whereCondition, "api");
        }
        public static DataTable IsInsuredDetailExistOrNot(string enquiryid)
        {
            return Post_Database.GetRecordFromTable("*", "T_Health_Insured", "enquiryid='" + enquiryid + "'", "api");
        }
        public static bool InsertInsuredDetails(string enquiryid, string productid, string companyid, int relationid, string relationname, string title, string firstname, string lastname, string dob, int height, int weight, bool isinsured, string issame, string occupationid, string isengagemanuallabour, string isengagewintersports, string isillness, string illnessdesc, string engageManualLabourDesc, string engageWinterSportDesc)
        {
            string fileds = "enquiryid,productid,companyid,relationid,relationname,title,firstname,lastname,dob,height,weight,isinsured,issame,occupationId,engageManualLabour,engageWinterSports,illness,illnessdesc,engageManualLabourDesc,engageWinterSportDesc";
            string fieldValue = "'" + enquiryid + "','" + productid + "','" + companyid + "','" + relationid + "','" + relationname + "','" + title + "','" + firstname + "','" + lastname + "','" + dob + "','" + height + "','" + weight + "'," + (isinsured ? "1" : "0") + "," + (isinsured ? "1" : "0") + ",'" + occupationid + "'," + (Convert.ToBoolean(isengagemanuallabour.ToLower()) ? "1" : "0") + "," + (Convert.ToBoolean(isengagewintersports.ToLower()) ? "1" : "0") + ", " + (Convert.ToBoolean(isillness.ToLower()) ? "1" : "0") + ",'" + illnessdesc + "','" + engageManualLabourDesc + "','" + engageWinterSportDesc + "'";
            return Post_Database.InsertRecordToTable(fileds, fieldValue, "T_Health_Insured", "api");
        }
        public static bool UpdateInsuredDetails(string enquiryid, string productid, string companyid, int relationid, string relationname, string title, string firstname, string lastname, string dob, int height, int weight, bool isinsured, string issame, string occupationid, string isengagemanuallabour, string isengagewintersports, string isillness, string illnessdesc, string engageManualLabourDesc, string engageWinterSportDesc, int id)
        {
            string fieldsWithValues = "enquiryid='" + enquiryid + "',productid='" + productid + "',companyid='" + companyid + "',relationid='" + relationid + "',relationname='" + relationname + "',title='" + title + "',firstname='" + firstname + "',lastname='" + lastname + "',dob='" + dob + "',height='" + height + "',weight='" + weight + "',isinsured=" + (isinsured ? "1" : "0") + ",occupationId='" + occupationid + "',engageManualLabour=" + (Convert.ToBoolean(isengagemanuallabour.ToLower()) ? "1" : "0") + ",engageWinterSports=" + (Convert.ToBoolean(isengagewintersports.ToLower()) ? "1" : "0") + ",illness=" + (Convert.ToBoolean(isillness.ToLower()) ? "1" : "0") + ",illnessdesc='" + illnessdesc + "',engageManualLabourDesc='" + engageManualLabourDesc + "',engageWinterSportDesc='" + engageWinterSportDesc + "'";
            return Post_Database.UpdateRecordIntoAnyTable("T_Health_Insured", fieldsWithValues, "enquiryid='" + enquiryid + "' and id=" + id + "", "api");
        }
        public static bool UpdateNomineeDetail(string title, string firstname, string lastname, string dob, int age, string relation, string enquiryid)
        {
            string fieldsWithValue = "ntitle='" + title + "',nfirstname='" + firstname + "',nlastname='" + lastname + "',ndob='" + dob + "',nage='" + age + "',nrelation='" + relation + "'";
            return Post_Database.UpdateRecordIntoAnyTable("T_Health_Proposal", fieldsWithValue, "enquiryid='" + enquiryid + "'", "api");
        }
        public static DataTable GetInsuredDetails(string enquiryid, string productid = "", string companyid = "")
        {
            string wherecon = "enquiryid='" + enquiryid + "'";
            if (!string.IsNullOrEmpty(productid.Trim()))
            {
                wherecon = wherecon + " and productid='" + productid + "'";
            }
            if (!string.IsNullOrEmpty(companyid.Trim()))
            {
                wherecon = wherecon + " and companyid='" + companyid + "'";
            }
            wherecon = wherecon + " order by id";
            return Post_Database.GetRecordFromTable("*", "T_Health_Insured", wherecon, "api");
        }
        public static bool UpdateQuestionerDetails(string questionjson, string enquiryid, string firstname = "", string lastnane = "", string id = "")
        {
            string fieldsWithValue = "medicalhistory='" + questionjson.Replace("'", "''") + "'";
            string whereCondition = "enquiryid='" + enquiryid + "'";
            if (!string.IsNullOrEmpty(firstname.Trim()))
            {
                whereCondition = whereCondition + " and firstname='" + firstname + "'";
            }
            if (!string.IsNullOrEmpty(lastnane.Trim()))
            {
                whereCondition = whereCondition + " and lastname='" + lastnane + "'";
            }
            if (!string.IsNullOrEmpty(id.Trim()))
            {
                whereCondition = whereCondition + " and id='" + id + "'";
            }
            return Post_Database.UpdateRecordIntoAnyTable("T_Health_Insured", fieldsWithValue, whereCondition, "api");
        }
        public static DataTable GetPaymentDetail(string enquiryid)
        {
            string wherecon = "enquiryid='" + enquiryid + "' order by id";
            return Post_Database.GetRecordFromTable("*", "T_Health_Payment", wherecon, "api");
        }
        public static bool InsertPaymentDetail(string enquiry_id, string referenceId, string premium, string totalPremium, string proposalNum, string productDetailId, string companyId, string paymenturl, string servicetax)
        {
            string fileds = "enquiryId,pp_referenceId,company,pp_companyId,pp_premium,pp_totalPremium,pp_proposalNum,pp_productDetailId,pp_rowpaymenturl,logoUrl,pp_serviceTax";
            string fieldValue = "'" + enquiry_id + "','" + referenceId + "',(select CompanyName from [seeinsuredapi_test].[dbo].[tbl_Company] where CompanyId=" + companyId + "),'" + companyId + "','" + premium + "','" + totalPremium + "','" + proposalNum + "','" + productDetailId + "','" + paymenturl + "',(select logoUrl from [seeinsuredapi_test].[dbo].[tbl_Company] where CompanyId=" + companyId + "),'" + servicetax + "'";
            return Post_Database.InsertRecordToTable(fileds, fieldValue, "T_Health_Payment", "api");
        }
        public static bool UpdatePaymentDetail(string enquiry_id, string referenceId, string premium, string totalPremium, string proposalNum, string productDetailId, string companyId, string paymenturl, string servicetax)
        {
            string fieldsWithValue = "pp_referenceId='" + referenceId + "',pp_premium='" + premium + "',pp_totalPremium='" + totalPremium + "',pp_proposalNum='" + proposalNum + "',pp_productDetailId='" + productDetailId + "',pp_companyId='" + companyId + "',pp_rowpaymenturl='" + paymenturl + "',pp_serviceTax='" + servicetax + "',pay_proposal_Number='" + proposalNum + "'";
            string whereCondition = "enquiryid='" + enquiry_id + "'";
            return Post_Database.UpdateRecordIntoAnyTable("T_Health_Payment", fieldsWithValue, whereCondition, "api");
        }
        public static bool UpdateProposalPaymentDetail(string p_proposalNum, string p_totalPremium, string companyId, string p_rowpaymenturl, string enquiryid, string p_referenceId)
        {
            string p_fieldsWithValue = "p_proposalNum='" + p_proposalNum + "',p_totalPremium='" + p_totalPremium + "',logoUrl=(select logoUrl from [seeinsuredapi_test].[dbo].[tbl_Company] where CompanyId=" + companyId + "),p_rowpaymenturl='" + p_rowpaymenturl + "'";
            if (companyId == "2")
            {
                p_fieldsWithValue = p_fieldsWithValue + ",p_referenceId='" + p_proposalNum + "',pay_proposal_Number='" + p_proposalNum + "'";
            }
            else
            {
                p_fieldsWithValue = p_fieldsWithValue + ",p_referenceId = '" + p_referenceId + "'";
            }
            string p_whereCondition = "enquiryid='" + enquiryid + "'";
            return Post_Database.UpdateRecordIntoAnyTable("T_Health_Proposal", p_fieldsWithValue, p_whereCondition, "api");
        }
        public static bool UpdatePaymentDetail(string p_proposalNum, string enquiryid)
        {
            return Post_Database.UpdateRecordIntoAnyTable("T_Health_Payment", "pp_referenceId='" + p_proposalNum + "'", "enquiryid='" + enquiryid + "'", "api");
        }
        public static DataTable GetOccupationDetail(string compantId)
        {
            return Post_Database.GetRecordFromTable("*", "tbl_OccupationMaster", "CompanyID = '" + compantId + "'", "api");
        }
        public static DataTable GetNominee_CommonDetails(string companyid, string type)
        {
            string wherecon = "CompanyId=" + companyid + " and Type='" + type + "' order by Relationship";
            return Post_Database.GetRecordFromTable("*", "tbl_Nominee_Common", wherecon, "api");
        }
        public static DataTable GetAnnualIncomeDetails(string enquiryid, string companyid = "")
        {
            string wherecon = "";
            if (!string.IsNullOrEmpty(companyid))
            {
                wherecon = "CompanyId='" + companyid + "'";
            }
            else
            {
                wherecon = "CompanyId in (select companyid from T_Health_Proposal where enquiryid='" + enquiryid + "')";
            }

            return Post_Database.GetRecordFromTable("*", "Master_Annual_Income", wherecon, "api");
        }
        public static bool UpdateHealthPolicyPdfLink(string enquiryid, string policypdflink, string websiteUrl)
        {
            string fieldsWithValue = "policypdflink='" + (websiteUrl + policypdflink) + "'";
            return Post_Database.UpdateRecordIntoAnyTable("T_Health_Proposal", fieldsWithValue, "enquiryid='" + enquiryid + "'", "api");
        }
        public static bool UpdateCarePayment_HealthProposal(string enquiryid, string policyNumber)
        {
            return Post_Database.UpdateRecordIntoAnyTable("T_Health_Proposal", "pay_policy_Number='" + policyNumber + "'", "enquiryid='" + enquiryid + "'", "api");
        }
        public static bool UpdatePaymentDate_HealthProposal(string enquiryid)
        {
            return Post_Database.UpdateRecordIntoAnyTable("T_Health_Proposal", "pay_paymentdate=getdate()", "enquiryid='" + enquiryid + "'", "api");
        }
        //public static bool UpdateCarePaymentDetails(string policyNumber, string transactionRefNum, string uwDecision, string errorFlag, string errorMsg, string status, string enquiryid, string purchaseToken = "")
        //{
        //    string fieldsWithValue = "cc_policyNumber='" + policyNumber + "',cc_transactionRefNum='" + transactionRefNum + "',cc_uwDecision='" + uwDecision + "',cc_errorFlag='" + errorFlag + "',cc_errorMsg='" + errorMsg + "',Star_PurchaseToken='" + purchaseToken + "',Status='" + status + "',payment_status='1',UpdatedDate=getdate()";
        //    return Post_Database.UpdateRecordIntoAnyTable("T_Health_Payment", fieldsWithValue, "enquiryid='" + enquiryid + "'", "api");
        //}
        public static bool UpdateRowRequest_EnquiryResponse(string requestJson, string enquiryid)
        {
            return Post_Database.UpdateRecordIntoAnyTable("T_Enquiry_Response", "perposalrequest='" + requestJson.Replace("'", "''") + "'", "enquiry_id='" + enquiryid + "'", "api");
        }
        public static bool UpdateResponse_EnquiryResponse(string perposalresponse, string enquiryid)
        {
            return Post_Database.UpdateRecordIntoAnyTable("T_Enquiry_Response", "perposalresponse='" + perposalresponse.Replace("'", "''") + "'", "enquiry_id='" + enquiryid + "'", "api");
        }
        public static bool UpdateStartEndPolicyDate(string enquiryid, string updateDel)
        {
            return Post_Database.UpdateRecordIntoAnyTable("T_Health_Proposal", updateDel, "enquiryid='" + enquiryid + "'", "api");
        }
        public static bool UpdateRaw_Paymenturl_EnquiryResponse(string raw_paymenturl, string enquiryid)
        {
            return Post_Database.UpdateRecordIntoAnyTable("T_Enquiry_Response", "paymentrequest='" + raw_paymenturl + "'", "enquiry_id='" + enquiryid + "'", "api");
        }
        public static bool UpdatePaymentResponse_EnquiryResponse(string payresponse, string enquiryid)
        {
            return Post_Database.UpdateRecordIntoAnyTable("T_Enquiry_Response", "paymentresponse='" + payresponse + "'", "enquiry_id='" + enquiryid + "'", "api");
        }
        public static bool UpdateQuestionnaireRequest_EnquiryResponse(string questionaryrequest, string enquiryid)
        {
            return Post_Database.UpdateRecordIntoAnyTable("T_Enquiry_Response", "questionaryrequest='" + questionaryrequest + "'", "enquiry_id='" + enquiryid + "'", "api");
        }
        public static bool UpdateQuestionnaireResponse_EnquiryResponse(string questionaryrespo, string enquiryid)
        {
            return Post_Database.UpdateRecordIntoAnyTable("T_Enquiry_Response", "questionaryresponse='" + questionaryrespo + "'", "enquiry_id='" + enquiryid + "'", "api");
        }
        public static bool UpdateProposalAndPolicyRequset_EnquiryResponse(string papRequest, string enquiryid)
        {
            return Post_Database.UpdateRecordIntoAnyTable("T_Enquiry_Response", "ProposalAndPolicyReq='" + papRequest + "'", "enquiry_id='" + enquiryid + "'", "api");
        }
        public static bool UpdateProposalAndPolicyResponse_EnquiryResponse(string papResponse, string enquiryid)
        {
            return Post_Database.UpdateRecordIntoAnyTable("T_Enquiry_Response", "ProposalAndPolicyRespo='" + papResponse + "'", "enquiry_id='" + enquiryid + "'", "api");
        }
        public static bool UpdateRowRequest_HealthProposal(string requestJson, string enquiryid)
        {
            return Post_Database.UpdateRecordIntoAnyTable("T_Health_Proposal", "proposalreq='" + requestJson.Replace("'", "''") + "'", "enquiryid='" + enquiryid + "'", "api");
        }
        public static bool UpdateResponse_HealthProposal(string perposalresponse, string enquiryid)
        {
            return Post_Database.UpdateRecordIntoAnyTable("T_Health_Proposal", "perposalrespo='" + perposalresponse.Replace("'", "''") + "'", "enquiryid='" + enquiryid + "'", "api");
        }
        public static bool UpdatePaymentDetail_HealthProposal(string proposal_Number, string pay_policy_Number, string enquiryid)
        {
            return Post_Database.UpdateRecordIntoAnyTable("T_Health_Proposal", "pay_proposal_Number='" + proposal_Number + "', pay_policy_Number='" + pay_policy_Number + "'", "enquiryid='" + enquiryid + "'", "api");
        }
        public static DataTable GetHealthPaymentDetail(string enquiryid)
        {
            string wherecon = "enquiryid='" + enquiryid + "' order by id";
            return Post_Database.GetRecordFromTable("*", "T_Health_Payment", wherecon, "api");
        }
        public static bool UpdateCarePaymentDetails(string policyNumber, string transactionRefNum, string uwDecision, string errorFlag, string errorMsg, string status, string enquiryid, string purchaseToken = "")
        {
            try
            {
                bool issuccess = Post_Database.UpdateRecordIntoAnyTable("T_Health_Proposal", "pay_policy_Number='" + policyNumber + "'", ("enquiryid='" + enquiryid + "'"), "api");

                string fieldsWithValue = "cc_policyNumber='" + policyNumber + "',cc_transactionRefNum='" + transactionRefNum + "',cc_uwDecision='" + uwDecision + "',cc_errorFlag='" + errorFlag + "',cc_errorMsg='" + errorMsg + "',Star_PurchaseToken='" + purchaseToken + "',Status='" + status + "',payment_status='1',UpdatedDate=getdate()";
                if (Post_Database.UpdateRecordIntoAnyTable("T_Health_Payment", fieldsWithValue, ("enquiryid='" + enquiryid + "'"), "api"))
                {
                    return Post_Database.UpdateRecordIntoAnyTable("T_Health_Proposal", "pay_paymentdate=getdate()", ("enquiryid='" + enquiryid + "'"), "api");
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }
        public static DataTable GetHealth_ProposalByEnqId(string enquiryid)
        {
            string fileds = "*";
            string tablename = "T_Health_Proposal";
            return Post_Database.GetRecordFromTable(fileds, tablename, "enquiryid='" + enquiryid + "'", "api");
        }
        public static bool UpdateAdityBirlaPaymentDetails(string amount, string transactionRefNum, string uwDecision, string errorFlag, string errorMsg, string status, string enquiryid, string txnDate, string quoteId)
        {
            try
            {
                //bool issuccess = ConnectToDataBase.UpdateRecordIntoAnyTable("T_Health_Proposal", "pay_policy_Number='" + policyNumber + "'", ("enquiryid='" + enquiryid + "'"), "api");

                string fieldsWithValue = "pp_totalPremium='" + amount + "',cc_transactionRefNum='" + transactionRefNum + "',cc_uwDecision='" + uwDecision + "',cc_errorFlag='" + errorFlag + "',cc_errorMsg='" + errorMsg + "',Status='" + status + "',payment_status='1',ab_QuoteId='" + quoteId + "',UpdatedDate='" + txnDate + "'";
                if (Post_Database.UpdateRecordIntoAnyTable("T_Health_Payment", fieldsWithValue, ("enquiryid='" + enquiryid + "'"), "api"))
                {
                    return Post_Database.UpdateRecordIntoAnyTable("T_Health_Proposal", "pay_paymentdate=getdate()", ("enquiryid='" + enquiryid + "'"), "api");
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }
        public static bool UpdateAdityBirlaSaveProposal(string reqJson, string enquiryid, string type)
        {
            bool issuccess = false;
            try
            {
                if (type == "request")
                {
                    issuccess = Post_Database.UpdateRecordIntoAnyTable("T_Enquiry_Response", "SaveProposalRequest='" + reqJson + "'", ("enquiry_Id='" + enquiryid + "'"), "api");
                }
                else
                {
                    issuccess = Post_Database.UpdateRecordIntoAnyTable("T_Enquiry_Response", "SaveProposalRespo='" + reqJson + "'", ("enquiry_Id='" + enquiryid + "'"), "api");
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return issuccess;
        }
        public static bool UpdateAdityBirlaSaveProposal_Respo(string referenceId, string proposalNum, string receiptAmount, string receiptNumber, string policyNumber, string policyStatus, string inquiry_id)
        {
            bool issuccess = false;
            try
            {
                if (Post_Database.UpdateRecordIntoAnyTable("T_Health_Proposal", "p_proposalNum='" + proposalNum + "',pay_proposal_Number='" + proposalNum + "',pay_policy_Number='" + policyNumber + "'", ("enquiryid='" + inquiry_id + "'"), "api"))
                {
                    issuccess = Post_Database.UpdateRecordIntoAnyTable("T_Health_Payment", "cc_policyNumber='" + policyNumber + "',abhi_receiptAmount='" + receiptAmount + "',abhi_receiptNumber='" + receiptNumber + "',abhi_policyStatus='" + policyStatus + "',abhi_referenceId='" + referenceId + "'", ("enquiryid='" + inquiry_id + "'"), "api");
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return issuccess;
        }
        public static DataTable GetAddLessAddon(string enquiryid)
        {
            return Post_Database.GetRecordFromTable("*", "T_HealthAddon", "inquiryid='" + enquiryid + "' and status=1", "api");
        }
        public static DataTable GetSelectedAddLessAddon(string enquiryid)
        {
            return Post_Database.GetRecordFromTable("*", "T_HealthAddon", "inquiryid='" + enquiryid + "' and status=1 and isselected=1", "api");
        }
        public static DataTable GetAddLessAddon(string enquiryid, string amount, string period)
        {
            return Post_Database.GetRecordFromTable("*", "T_HealthAddon", "inquiryid='" + enquiryid + "' and PlanAmount='" + amount + "' and PlanPeriod='" + period + "' and status=1", "api");
        }
        public static DataTable GetAddLessAddon(string enquiryid, string addOnsId, string addOns, string code, string value, string calculation, string addOnValue, string productDetailID, string planAmount, string planPeriod)
        {
            string p_wherecon = "inquiryid='" + enquiryid + "' and addOnsId='" + addOnsId + "' and addOns='" + addOns + "' and code='" + code + "' and value='" + value + "' and calculation='" + calculation + "' and addOnValue='" + addOnValue + "' and productDetailID='" + productDetailID + "' and status=1 and PlanAmount='" + planAmount + "' and PlanPeriod='" + planPeriod + "'";
            return Post_Database.GetRecordFromTable("*", "T_HealthAddon", p_wherecon, "api");
        }
        public static bool AddLessAddon(string[] addonlist)
        {
            bool issuccess = false;
            try
            {
                string p_wherecon = "inquiryid='" + addonlist[7] + "' and addOnsId='" + addonlist[0] + "' and addOns='" + addonlist[1] + "' and code='" + addonlist[2] + "' and value='" + addonlist[3] + "' and calculation='" + addonlist[4] + "' and addOnValue='" + addonlist[5] + "' and productDetailID='" + addonlist[6] + "' and PlanAmount='" + addonlist[13] + "' and PlanPeriod='" + addonlist[14] + "'";
                DataTable dtAddonExist = Post_Database.GetRecordFromTable("*", "T_HealthAddon", p_wherecon, "api");
                if (dtAddonExist != null && dtAddonExist.Rows.Count > 0)
                {
                    issuccess = Post_Database.UpdateRecordIntoAnyTable("T_HealthAddon", "updateddate=getdate(),status=" + addonlist[8], p_wherecon, "api");
                }
                else
                {
                    string fileds = "inquiryid,addOnsId,addOns,code,value,calculation,addOnValue,productDetailID,status,PlanAmount,PlanPeriod,BasePremium";
                    string fieldValue = "'" + addonlist[7] + "','" + addonlist[0] + "','" + addonlist[1] + "','" + addonlist[2] + "','" + addonlist[3] + "','" + addonlist[4] + "'" +
                        ",'" + addonlist[5] + "','" + addonlist[6] + "'," + addonlist[8] + ",'" + addonlist[13] + "','" + addonlist[14] + "','" + addonlist[9] + "'";
                    issuccess = Post_Database.InsertRecordToTable(fileds, fieldValue, "T_HealthAddon", "api");
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return issuccess;
        }
        public static DataSet GetProposalEmailDetail(string enquiryid)
        {
            return Post_Database.GetProposalEmailDetail(enquiryid);
        }
        public static bool UpdateCommission_PassBook(string enquiryid, string premium)
        {
            return Post_Database.UpdateCommission_PassBook(enquiryid, premium, "api");
        }
    }
}
