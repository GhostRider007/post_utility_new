﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Post_Utility.Database;

namespace Post_Utility.Common
{
    public static class AutoCompleteHelper
    {
        public static DataTable GetAgentDetail_Like(string agencyName = "", string agencyId = "", string status = "", string columns = "")
        {
            string fields = !string.IsNullOrEmpty(columns) ? columns : "*";
            string wherecon = string.Empty;
            if (!string.IsNullOrEmpty(agencyName.Trim()))
            {
                wherecon = !string.IsNullOrEmpty(wherecon) ? (wherecon + " and AgencyName like '%" + agencyName + "%'") : " AgencyName like '%" + agencyName + "%'";
            }
            if (!string.IsNullOrEmpty(agencyId.Trim()))
            {
                wherecon = !string.IsNullOrEmpty(wherecon) ? (wherecon + " and AgencyId=" + agencyId) : " AgencyId=" + agencyId;
            }
            if (!string.IsNullOrEmpty(status.Trim()))
            {
                wherecon = !string.IsNullOrEmpty(wherecon) ? (wherecon + " and status='" + status + "'") : " status='" + status + "'";
            }
            
            return Post_Database.GetRecordFromTable(fields, "aspnet_user", wherecon, "api");
        }
    }
}
