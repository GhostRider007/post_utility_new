﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Post_Utility.Common
{
    public static class Common_Utility
    {
        public static string IndianMoneyFormat(string fare)
        {
            decimal parsed = decimal.Parse((Math.Ceiling(Convert.ToDecimal(fare)).ToString()), CultureInfo.InvariantCulture);
            CultureInfo hindi = new CultureInfo("hi-IN");
            return string.Format(hindi, "{0:c}", parsed).Replace("₹", "₹ ");
        }
        public static string ConvertDateYYMMDD(string date)
        {
            string rutDate = date;
            if (!string.IsNullOrEmpty(date))
            {
                string[] splitDate = date.Split('/');
                rutDate = splitDate[2] + "-" + splitDate[1] + "-" + splitDate[0];
            }
            return rutDate;
        }
        public static string ConvertDateToISOFormate(string date)
        {
            string rutDate = date;
            if (!string.IsNullOrEmpty(date))
            {
                string[] splitDate = date.Split('/');
                rutDate = splitDate[2] + "-" + splitDate[1] + "-" + splitDate[0];

                DateTime dtCDate = Convert.ToDateTime(rutDate);
                rutDate = dtCDate.ToUniversalTime().ToString("yyyy-MM-ddTHH:mm:ss.sssZ");
            }
            return rutDate;
        }
        public static int CharactersCount(string input)
        {
            int Count = 0;

            try
            {
                //char charCout = Convert.ToChar(input);
                foreach (char chr in input)
                {
                    Count++;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return Count;
        }
    }
}
