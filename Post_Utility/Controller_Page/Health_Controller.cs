﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Post_Utility.Model;
using Post_Utility.Common;
using System.Data;
using Post_Utility.Health;
using Post_Utility.Utility;

namespace Post_Utility.Controller_Page
{
    public static class Health_Controller
    {
        public static Dictionary<string, string> StarHealthConfirmationSection(string purchaseToken, string enqId)
        {
            Dictionary<string, string> rutStar = new Dictionary<string, string>();

            PaymentSuccessDel starpayDel = new PaymentSuccessDel();
            if (!string.IsNullOrEmpty(purchaseToken))
            {
                starpayDel.Status = "Success";
                rutStar.Add("status", "Success");
                starpayDel.purchaseToken = purchaseToken;
                rutStar.Add("purchaseToken", purchaseToken);
                if (!string.IsNullOrEmpty(enqId))
                {
                    starpayDel.enquiryid = enqId;
                    rutStar.Add("enquiryid", enqId);
                    if (!string.IsNullOrEmpty(starpayDel.enquiryid))
                    {
                        DataTable dtPayment = Post_Health.GetHealthPaymentDetail(starpayDel.enquiryid);
                        if (dtPayment != null && dtPayment.Rows.Count > 0)
                        {
                            bool payment_status = !string.IsNullOrEmpty(dtPayment.Rows[0]["payment_status"].ToString()) ? Convert.ToBoolean(dtPayment.Rows[0]["payment_status"].ToString()) : false;
                            if (!payment_status)
                            {
                                bool issuccess = Post_Health.UpdateCarePaymentDetails("", "", "", "", "", "success", starpayDel.enquiryid, purchaseToken);
                            }

                            DataTable dtProposal = Post_Health.GetHealth_ProposalByEnqId(starpayDel.enquiryid);
                            if (dtProposal != null && dtProposal.Rows.Count > 0)
                            {
                                string pay_proposal_Number = dtProposal.Rows[0]["pay_proposal_Number"].ToString();
                                string p_proposalNum = dtProposal.Rows[0]["p_proposalNum"].ToString();

                                //starpayDel.imgurl = "http://api.seeinsured.com" + perposalDel.logoUrl;
                                starpayDel.imgurl = dtProposal.Rows[0]["logoUrl"] != null ? dtProposal.Rows[0]["logoUrl"].ToString() : string.Empty;
                                starpayDel.companyid = dtProposal.Rows[0]["companyid"] != null ? dtProposal.Rows[0]["companyid"].ToString() : string.Empty;
                                starpayDel.premium = Common_Utility.IndianMoneyFormat(dtPayment.Rows[0]["pp_totalPremium"].ToString().Replace("INR", ""));
                                starpayDel.proposalNum = (!string.IsNullOrEmpty(pay_proposal_Number) ? pay_proposal_Number : p_proposalNum);

                                rutStar.Add("imgurl", starpayDel.imgurl);
                                rutStar.Add("companyid", starpayDel.companyid);
                                rutStar.Add("premium", starpayDel.premium);
                                rutStar.Add("proposalNum", starpayDel.proposalNum);
                            }
                        }
                    }
                }
            }
            else
            {
                starpayDel.Status = "Failed";
                rutStar.Add("status", "Failed");
                starpayDel.purchaseToken = purchaseToken;
                rutStar.Add("purchaseToken", purchaseToken);
            }
            return rutStar;
        }
        public static Dictionary<string, string> CareHealthConfirmationSection(string policyNumber, string transactionRefNum, string uwDecision, string errorFlag, string errorMsg, string Status, string enquiryid, string Inquery_id)
        {
            Dictionary<string, string> rutCare = new Dictionary<string, string>();

            try
            {
                PaymentSuccessDel caresuccess = new PaymentSuccessDel();
                caresuccess.enquiryid = Inquery_id;
                if (!string.IsNullOrEmpty(uwDecision))
                {
                    if (uwDecision.ToLower().Trim().Contains("inforce"))
                    {
                        caresuccess.Status = "Successful";
                    }
                    else if (uwDecision.ToLower().Trim().Contains("pending"))
                    {
                        caresuccess.Status = "Successful with Underwriting Scenario";
                    }
                    else
                    {
                        caresuccess.Status = "Failure";
                    }
                }
                else
                {
                    caresuccess.Status = "N/A";
                }

                rutCare.Add("status", caresuccess.Status);

                if (!string.IsNullOrEmpty(caresuccess.enquiryid))
                {
                    bool issuccess = Post_Health.UpdateCarePaymentDetails(policyNumber, transactionRefNum, uwDecision, errorFlag, errorMsg, caresuccess.Status, caresuccess.enquiryid);
                    DataTable dtProposal = Post_Health.GetHealth_ProposalByEnqId(caresuccess.enquiryid);
                    if (dtProposal != null && dtProposal.Rows.Count > 0)
                    {
                        string pay_proposal_Number = dtProposal.Rows[0]["pay_proposal_Number"].ToString();
                        string p_proposalNum = dtProposal.Rows[0]["p_proposalNum"].ToString();

                        caresuccess.imgurl = dtProposal.Rows[0]["logoUrl"] != null ? dtProposal.Rows[0]["logoUrl"].ToString() : string.Empty;
                        caresuccess.companyid = dtProposal.Rows[0]["companyid"] != null ? dtProposal.Rows[0]["companyid"].ToString() : string.Empty;
                        caresuccess.premium = Common_Utility.IndianMoneyFormat(dtProposal.Rows[0]["row_totalPremium"].ToString().Replace("INR", ""));
                        caresuccess.proposalNum = (!string.IsNullOrEmpty(pay_proposal_Number) ? pay_proposal_Number : p_proposalNum);

                        rutCare.Add("imgurl", caresuccess.imgurl);
                        rutCare.Add("companyid", caresuccess.companyid);
                        rutCare.Add("premium", caresuccess.premium);
                        rutCare.Add("proposalNum", caresuccess.proposalNum);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return rutCare;
        }
        public static Dictionary<string, string> AdityBirlaHealthConfirmationSection(string amount, string TxRefNo, string TxStatus, string TxMsg, string Status, string enquiryid, string txnDateTime, string QuoteId, string Inquery_id)
        {
            Dictionary<string, string> rutAdityBirla = new Dictionary<string, string>();

            try
            {
                PaymentSuccessDel caresuccess = new PaymentSuccessDel();

                caresuccess.enquiryid = Inquery_id;
                if (!string.IsNullOrEmpty(TxStatus))
                {
                    if (TxStatus.ToLower().Trim().Contains("success"))
                    {
                        caresuccess.Status = "Successful";
                    }
                    else if (TxStatus.ToLower().Trim().Contains("pending"))
                    {
                        caresuccess.Status = "Pending";
                    }
                    else
                    {
                        caresuccess.Status = "Failure";
                    }
                }
                else
                {
                    caresuccess.Status = "N/A";
                }

                rutAdityBirla.Add("status", caresuccess.Status);

                if (!string.IsNullOrEmpty(caresuccess.enquiryid))
                {
                    bool issuccess = Post_Health.UpdateAdityBirlaPaymentDetails(amount, TxRefNo, TxStatus, TxMsg, TxMsg, caresuccess.Status, caresuccess.enquiryid, txnDateTime, QuoteId);

                    DataTable dtProposal = Post_Health.GetHealth_ProposalByEnqId(caresuccess.enquiryid);
                    if (dtProposal != null && dtProposal.Rows.Count > 0)
                    {
                        string companyid = dtProposal.Rows[0]["companyid"].ToString();
                        string productid = dtProposal.Rows[0]["productid"].ToString();

                        string pay_proposal_Number = dtProposal.Rows[0]["pay_proposal_Number"].ToString();
                        string p_proposalNum = dtProposal.Rows[0]["p_proposalNum"].ToString();

                        caresuccess.imgurl = dtProposal.Rows[0]["logoUrl"] != null ? dtProposal.Rows[0]["logoUrl"].ToString() : string.Empty;
                        caresuccess.companyid = dtProposal.Rows[0]["companyid"] != null ? dtProposal.Rows[0]["companyid"].ToString() : string.Empty;
                        caresuccess.premium = Common_Utility.IndianMoneyFormat(dtProposal.Rows[0]["row_totalPremium"].ToString().Replace("INR", ""));
                        caresuccess.proposalNum = (!string.IsNullOrEmpty(pay_proposal_Number) ? pay_proposal_Number : p_proposalNum);

                        rutAdityBirla.Add("imgurl", caresuccess.imgurl);
                        rutAdityBirla.Add("companyid", caresuccess.companyid);
                        rutAdityBirla.Add("premium", caresuccess.premium);
                        rutAdityBirla.Add("proposalNum", caresuccess.proposalNum);
                        rutAdityBirla.Add("productid", productid);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return rutAdityBirla;
        }
        public static string SetPremiumBreakup(string basePremium, string premium, string discount, string serviceTax, string totalPremium, string period, string enquiryid)
        {
            StringBuilder sbPBup = new StringBuilder();

            try
            {
                sbPBup.Append("<div class='col-sm-12' style='border-bottom: 1px solid #ccc;'>");
                sbPBup.Append("<div class=\"row\">");
                sbPBup.Append("<div class='col-sm-6'><label>Basic</label></div>");
                sbPBup.Append("<div class='col-sm-6' style='text-align: right;'><label>" + UtilityClass.IndianMoneyFormat(basePremium.Replace("INR", "")) + "</label></div>");
                sbPBup.Append("</div>");
                sbPBup.Append("</div>");

                string disAmt = (Convert.ToDouble(basePremium.Replace("INR", "")) - Convert.ToDouble(premium.Replace("INR", ""))).ToString();
                double disPer = Convert.ToDouble(discount.Replace("%", ""));

                sbPBup.Append("<div class='col-sm-12' style='border-bottom: 1px solid #ccc;'>");
                sbPBup.Append("<div class=\"row\">");
                sbPBup.Append("<div class='col-sm-6'><label>Discount (" + (disPer > 0 ? disPer.ToString() : "0.0") + " %)</label></div>");
                sbPBup.Append("<div class='col-sm-6' style='text-align: right;'><label>" + UtilityClass.IndianMoneyFormat(disAmt) + "</label></div>");
                sbPBup.Append("</div>");
                sbPBup.Append("</div>");

                sbPBup.Append("<div class='col-sm-12' style='border-bottom: 1px solid #ccc;'>");
                sbPBup.Append("<div class=\"row\">");
                sbPBup.Append("<div class='col-sm-6'><label>GST 18%</label></div>");
                sbPBup.Append("<div class='col-sm-6' style='text-align: right;'><label>" + UtilityClass.IndianMoneyFormat(serviceTax.Replace("INR", "")) + "</label></div>");
                sbPBup.Append("</div>");
                sbPBup.Append("</div>");

                DataTable dtAddon = Post_Health.GetAddLessAddon(enquiryid, totalPremium, period);
                if (dtAddon != null && dtAddon.Rows.Count > 0)
                {
                    for (int i = 0; i < dtAddon.Rows.Count; i++)
                    {
                        string addonValue = dtAddon.Rows[i]["value"].ToString();
                        string addoncalculation = dtAddon.Rows[i]["calculation"].ToString();
                        string addOnValue = dtAddon.Rows[i]["addOnValue"].ToString();
                        if (addonValue.Trim() == "fixed")
                        {
                            if (addoncalculation == "add")
                            {
                                totalPremium = (Math.Ceiling(Convert.ToDecimal(totalPremium) + Convert.ToDecimal(addOnValue))).ToString();
                            }
                            else
                            {
                                totalPremium = (Math.Ceiling(Convert.ToDecimal(totalPremium) - Convert.ToDecimal(addOnValue))).ToString();
                            }
                        }
                        else
                        {
                            addOnValue = ((Convert.ToDecimal(basePremium) * Convert.ToDecimal(addOnValue)) / 100).ToString();

                            if (addoncalculation == "add")
                            {
                                totalPremium = (Math.Ceiling((Convert.ToDecimal(totalPremium) + Convert.ToDecimal(addOnValue)))).ToString();
                            }
                            else
                            {
                                totalPremium = (Math.Ceiling(Convert.ToDecimal(totalPremium) - Convert.ToDecimal(addOnValue))).ToString();
                            }
                        }
                        sbPBup.Append("<div class='col-sm-12' style='border-bottom: 1px solid #ccc;'>");
                        sbPBup.Append("<div class=\"row\">");
                        sbPBup.Append("<div class='col-sm-6'><label>" + dtAddon.Rows[i]["addOns"].ToString() + "</label></div>");
                        sbPBup.Append("<div class='col-sm-6' style='text-align: right;'><label>" + UtilityClass.IndianMoneyFormat(addOnValue) + "</label></div>");
                        sbPBup.Append("</div>");
                        sbPBup.Append("</div>");
                    }
                }
                sbPBup.Append("<div class='col-sm-12'>");
                sbPBup.Append("<div class=\"row\">");
                sbPBup.Append("<div class='col-sm-6'><label>You'll Pay</label></div>");
                sbPBup.Append("<div class='col-sm-6' style='text-align: right;'><label>" + UtilityClass.IndianMoneyFormat(totalPremium.Replace("INR", "")) + "</label></div>");
                sbPBup.Append("</div>");
                sbPBup.Append("</div>");
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return sbPBup.ToString();
        }
        public static string AddonsHtml(int addOnsId, string addOns, string code, string value, string calculation, string addOnValue, string productDetailID, string inquiryid, string basePremium, string premium, double discountPercent, string serviceTax, string totalPremium, int period)
        {
            StringBuilder sbStr = new StringBuilder();

            try
            {
                string showaddOnValue = addOnValue;

                bool isSelected = false;
                DataTable dtAddon = Post_Health.GetAddLessAddon(inquiryid, addOnsId.ToString(), addOns, code, value, calculation, addOnValue, productDetailID, totalPremium, period.ToString());
                if (dtAddon != null && dtAddon.Rows.Count > 0)
                {
                    isSelected = true;
                }

                if (value.Trim() == "percentage")
                {
                    showaddOnValue = ((Convert.ToDecimal(basePremium) * Convert.ToDecimal(addOnValue)) / 100).ToString();
                }

                sbStr.Append("<div class='col-sm-4'>");
                sbStr.Append("<h6 class='checkbox-inline' style='margin-top: 8px;'>");
                sbStr.Append("<label class='seeingo' style='font-weight: bold !important;color: #455757 !important;'>" + addOns);
                sbStr.Append("<input type='checkbox' class='submenuclass chkAddon_Year" + period + "' id='chkAddon_" + addOnsId + "' name='chkAddon_" + addOnsId + "' data-addonsid='" + addOnsId + "' data-addons='" + addOns + "' data-code='" + code + "' data-value='" + value + "' data-calculation='" + calculation.Trim() + "' data-addonvalue='" + addOnValue + "' data-productdetailid='" + productDetailID + "' data-inquiryid='" + inquiryid + "' data-basepremium='" + basePremium + "' data-premium='" + premium + "' data-discountpercent='" + discountPercent.ToString() + "' data-serviceTax='" + serviceTax + "' data-totalpremium='" + totalPremium + "' data-period='" + period + "' onclick='AddLessAddon(" + addOnsId + ");' " + (isSelected == true ? "checked" : "") + " />");
                sbStr.Append("<span class='checkmark'></span></label>");
                sbStr.Append("<p style='margin-left: 30px; margin-top: 1px; font-weight: 100; color: #8a8a8a;'>" + UtilityClass.IndianMoneyFormat(showaddOnValue) + "</p>");
                sbStr.Append("</h6></div>");
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return sbStr.ToString();
        }
    }
}
