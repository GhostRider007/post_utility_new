﻿using Post_Utility.Database;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Post_Utility.Utility
{
    public static class EmailUtility
    {
        public static bool Send_HealthProposalDetails(string enquiryid, string pageurl)
        {
            try
            {
                string InsurerName = string.Empty;

                StringBuilder message = new StringBuilder();
                message.Append(pageurl);
                message.Replace("#Logo#", (ConfigUtility.WebsiteUrl + ConfigUtility.GetLogoLink));
                message.Replace("#Url#", ConfigUtility.WebsiteUrl);
                message.Replace("#WebsiteName#", ConfigUtility.WebsiteName);

                DataSet dtProposalDel = Post_Database.GetProposalEmailDetail(enquiryid);
                if (dtProposalDel != null && dtProposalDel.Tables.Count > 0)
                {
                    string emailTo = string.Empty;
                    string emailFrom = string.Empty;
                    string proposalNo = string.Empty;

                    DataTable dtAgencyDetail = dtProposalDel.Tables[0];
                    if (dtAgencyDetail != null && dtAgencyDetail.Rows.Count > 0)
                    {
                        emailFrom = dtAgencyDetail.Rows[0]["BusinessEmailID"].ToString();
                        message.Replace("#AgencyName#", dtAgencyDetail.Rows[0]["AgencyName"].ToString());
                        message.Replace("#Address#", dtAgencyDetail.Rows[0]["Address"].ToString());
                        message.Replace("#City#", dtAgencyDetail.Rows[0]["CityName"].ToString());
                        message.Replace("#Pin#", dtAgencyDetail.Rows[0]["pincode"].ToString());
                        message.Replace("#Phone#", dtAgencyDetail.Rows[0]["BusinessPhone"].ToString());
                        message.Replace("#EmailAgency#", dtAgencyDetail.Rows[0]["BusinessEmailID"].ToString());
                    }

                    DataTable dtProposalDetail = dtProposalDel.Tables[1];
                    if (dtProposalDetail != null && dtProposalDetail.Rows.Count > 0)
                    {
                        emailTo = dtProposalDetail.Rows[0]["emailid"].ToString();
                        message.Replace("#ProposarName#", (dtProposalDetail.Rows[0]["firstname"].ToString() + " " + dtProposalDetail.Rows[0]["lastname"].ToString() + ","));
                        message.Replace("#ProposalNo#", dtProposalDetail.Rows[0]["pay_proposal_Number"].ToString());
                        message.Replace("#CreatedDate#", dtProposalDetail.Rows[0]["createddate"].ToString());
                        message.Replace("#OrderID#", enquiryid);
                        message.Replace("#Validtill#", dtProposalDetail.Rows[0]["endon_date"].ToString());

                        message.Replace("#Title#", dtProposalDetail.Rows[0]["title"].ToString());
                        message.Replace("#Name#", (dtProposalDetail.Rows[0]["firstname"].ToString() + " " + dtProposalDetail.Rows[0]["lastname"].ToString()));
                        message.Replace("#DOB#", dtProposalDetail.Rows[0]["dob"].ToString());
                        message.Replace("#Mobile#", dtProposalDetail.Rows[0]["mobile"].ToString());
                        message.Replace("#Email#", dtProposalDetail.Rows[0]["emailid"].ToString());
                        message.Replace("#AnnualIncome#", dtProposalDetail.Rows[0]["annualincome"].ToString());
                        message.Replace("#Address#", (dtProposalDetail.Rows[0]["address1"].ToString() + ", " + dtProposalDetail.Rows[0]["landmark"].ToString() + ", " + dtProposalDetail.Rows[0]["landmark"].ToString()));
                        message.Replace("#City#", dtProposalDetail.Rows[0]["cityname"].ToString());
                        message.Replace("#State#", dtProposalDetail.Rows[0]["statename"].ToString());
                        message.Replace("#PinCode#", dtProposalDetail.Rows[0]["pincode"].ToString());

                        message.Replace("#NomineeName#", (dtProposalDetail.Rows[0]["ntitle"].ToString() + " " + dtProposalDetail.Rows[0]["nfirstname"].ToString() + " " + dtProposalDetail.Rows[0]["nlastname"].ToString()));
                        message.Replace("#Relationship#", dtProposalDetail.Rows[0]["NomineeRelation"].ToString());
                        message.Replace("#DateOfBirth#", dtProposalDetail.Rows[0]["ndob"].ToString());

                        message.Replace("#ProductName#", dtProposalDetail.Rows[0]["Product"].ToString());
                        message.Replace("#Period#", dtProposalDetail.Rows[0]["periodfor"].ToString()+" Year");
                        message.Replace("#PolicyType#", dtProposalDetail.Rows[0]["Policy_Type_Text"].ToString());
                        message.Replace("#SumInsure#", dtProposalDetail.Rows[0]["suminsured"].ToString());
                        message.Replace("#Permimum#", "₹ " + dtProposalDetail.Rows[0]["p_totalPremium"].ToString());
                        message.Replace("#PaymentUrl#", dtProposalDetail.Rows[0]["p_paymenturl"].ToString());
                        message.Replace("#InsurerName#", dtProposalDetail.Rows[0]["Insurer"].ToString());
                    }

                    DataTable dtInsuredDetail = dtProposalDel.Tables[2];
                    if (dtInsuredDetail != null && dtInsuredDetail.Rows.Count > 0)
                    {
                        string HealthInsured = string.Empty;
                        for (int i = 0; i < dtInsuredDetail.Rows.Count; i++)
                        {
                            if (i == 0)
                            {
                                InsurerName = dtInsuredDetail.Rows[i]["title"].ToString() + " " + dtInsuredDetail.Rows[i]["firstname"].ToString() + " " + dtInsuredDetail.Rows[i]["lastname"].ToString();
                            }

                            HealthInsured += "<tr style='border-collapse: initial;border: 1px solid rgb(204,204,224);font-size: 14px;border-spacing: 0px;padding: 0px;'>";
                            HealthInsured += "<td colspan='1' style='text-align:center;border-right:1px solid #b3b3b3; border-bottom:1px solid #b3b3b3;color:#484848;padding: 10px;'>" + dtInsuredDetail.Rows[i]["relationname"].ToString() + "</td>";
                            HealthInsured += "<td colspan='1' style='text-align:center;border-right:1px solid #b3b3b3; border-bottom:1px solid #b3b3b3;color:#484848;padding: 10px;'>" + dtInsuredDetail.Rows[i]["title"].ToString() + "</td>";
                            HealthInsured += "<td colspan='1' style='text-align:center;border-right:1px solid #b3b3b3; border-bottom:1px solid #b3b3b3;color:#484848;padding: 10px;'>" + dtInsuredDetail.Rows[i]["firstname"].ToString() + " " + dtInsuredDetail.Rows[i]["lastname"].ToString() + "</td>";
                            HealthInsured += "<td colspan='1' style='text-align:center;border-right:1px solid #b3b3b3; border-bottom:1px solid #b3b3b3;color:#484848;padding: 10px;'>" + dtInsuredDetail.Rows[i]["dob"].ToString() + "</td>";
                            HealthInsured += "<td colspan='1' style='text-align:center;border-right:1px solid #b3b3b3; border-bottom:1px solid #b3b3b3;color:#484848;padding: 10px;'>" + dtInsuredDetail.Rows[i]["height"].ToString() + "</td>";
                            HealthInsured += "<td colspan='1' style='text-align:center;border-right:1px solid #b3b3b3; border-bottom:1px solid #b3b3b3;color:#484848;padding: 10px;'>" + dtInsuredDetail.Rows[i]["weight"].ToString() + "</td>";
                            HealthInsured += "</tr>";
                        }
                        message.Replace("#HealthInsured#", HealthInsured);
                    }

                    if (!string.IsNullOrEmpty(emailTo))
                    {
                        return SendEmail(emailFrom, emailTo, "Health Insurence : Proposal No : " + proposalNo, message.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            //string subject = "Reset your Password " + Config.WebsiteName;
            //Email.SendEmail(Config.AdminEmail, agency.BusinessEmailID, subject, message.ToString());

            return false;
        }
        private static bool SendEmail(string emailFrom, string emailTo, string subject, string body, string cc = "Rajnish@seeingo.com")
        {
            bool issent = false;
            try
            {
                using (MailMessage message = new MailMessage(emailFrom, emailTo, subject, body))
                {
                    if (cc != null) { message.CC.Add(cc); }
                    message.ReplyToList.Add(new MailAddress(emailFrom));
                    message.IsBodyHtml = true;
                    using (SmtpClient emailClient = new SmtpClient("smtp.gmail.com"))
                    {
                        emailClient.Port = 587;
                        emailClient.UseDefaultCredentials = true;
                        emailClient.EnableSsl = true;
                        emailClient.Credentials = new System.Net.NetworkCredential("support@seeinsured.com", "Insured@see");
                        emailClient.Send(message);
                    }
                }

                issent = true;
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return issent;
        }
    }
}
