﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Post_Utility.Utility
{
    public static class MotorApiUtility
    {
        public static string PostMotorAPI(string postType, string postUrl, string requestJson, string userid, string password)
        {
            string ReturnValue = string.Empty;
            StringBuilder sbResult = new StringBuilder();

            //ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;
            HttpWebRequest Http = (HttpWebRequest)WebRequest.Create(postUrl);

            try
            {
                var byteArray = Encoding.ASCII.GetBytes("krishna:123456");

                Http.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip,deflate");

                Http.Method = postType;
                byte[] lbPostBuffer = Encoding.UTF8.GetBytes(requestJson);
                Http.ContentLength = lbPostBuffer.Length;
                Http.Timeout = 130000; //5000 milliseconds == 5 seconds// 900000 milliseconds == 900 seconds- 15 mints
                Http.ContentType = "application/json";
                Http.Accept = "application/json";

                Http.Headers["Authorization"] = "Basic " + Convert.ToBase64String(byteArray);

                using (Stream PostStream = Http.GetRequestStream())
                {
                    PostStream.Write(lbPostBuffer, 0, lbPostBuffer.Length);
                }

                using (HttpWebResponse WebResponse = (HttpWebResponse)Http.GetResponse())
                {
                    if (WebResponse.StatusCode != HttpStatusCode.OK)
                    {
                        string message = String.Format("POST failed. Received HTTP {0}", WebResponse.StatusCode);
                        throw new ApplicationException(message);
                    }
                    else
                    {
                        Stream responseStream = WebResponse.GetResponseStream();
                        if ((WebResponse.ContentEncoding.ToLower().Contains("gzip")))
                        {
                            responseStream = new GZipStream(responseStream, CompressionMode.Decompress);
                        }
                        else if ((WebResponse.ContentEncoding.ToLower().Contains("deflate")))
                        {
                            responseStream = new DeflateStream(responseStream, CompressionMode.Decompress);
                        }
                        StreamReader reader = new StreamReader(responseStream, Encoding.Default);
                        sbResult.Append(reader.ReadToEnd());
                        ReturnValue = sbResult.ToString();
                        responseStream.Close();
                    }
                }
            }
            catch (WebException webEx)
            {
                HttpWebResponse httpResponse = webEx.Response as HttpWebResponse;
                using (Stream responseStream = httpResponse.GetResponseStream())
                {
                    using (StreamReader reader = new StreamReader(responseStream))
                    {
                        ReturnValue = GetWebResponseString(httpResponse);
                    }
                }
                ReturnValue = null;
            }
            finally
            {
                Http.Abort();
                Http = null;
            }

            return ReturnValue;
        }

        public static string GetWebResponseString(HttpWebResponse myHttpWebResponse)
        {
            StringBuilder rawResponse = new StringBuilder();
            string aa = "";
            Stream responseStream = myHttpWebResponse.GetResponseStream();
            Stream streamResponse = responseStream;
            using (responseStream)
            {
                if (myHttpWebResponse.ContentEncoding.ToLower().Contains("gzip") || myHttpWebResponse.ContentEncoding.ToLower().Contains("deflate"))
                {
                    if (myHttpWebResponse.ContentEncoding.ToLower().Contains("gzip"))
                    {
                        streamResponse = new GZipStream(streamResponse, CompressionMode.Decompress);
                    }
                    else if (myHttpWebResponse.ContentEncoding.ToLower().Contains("deflate"))
                    {
                        streamResponse = new DeflateStream(streamResponse, CompressionMode.Decompress);
                    }
                    using (StreamReader streamRead = new StreamReader(streamResponse))
                    {
                        //char[] readBuffer = new char[checked((IntPtr)myHttpWebResponse.ContentLength)];

                        //for (int count = streamRead.Read(readBuffer, 0, Convert.ToInt32(myHttpWebResponse.ContentLength)); count > 0; count = streamRead.Read(readBuffer, 0, Convert.ToInt32(myHttpWebResponse.ContentLength)))
                        //{
                        //    rawResponse.Append(new string(readBuffer, 0, count));
                        //}
                    }
                    aa = rawResponse.ToString();
                }
                else
                {
                    aa = (new StreamReader(streamResponse)).ReadToEnd().Trim();
                }
            }
            return aa;
        }
    }
}
