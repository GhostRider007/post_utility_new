﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Post_Utility.Utility
{
    public static class ConfigUtility
    {
        public static string GetLogoLink
        {
            get { return Convert.ToString(ConfigurationManager.AppSettings["weblogo"]); }
        }
        public static string WebsiteName
        {
            get { return Convert.ToString(ConfigurationManager.AppSettings["websitename"]); }
        }

        public static string WebsiteUrl
        {
            get { return Convert.ToString(ConfigurationManager.AppSettings["websiteurl"]); }
        }

        public static string MotorApiUrl
        {
            get { return Convert.ToString(ConfigurationManager.AppSettings["motorapiurl"]); }
        }

        public static string AdminEmail
        {
            get { return Convert.ToString(ConfigurationManager.AppSettings["adminemail"]); }
        }
        public static string AdminAPIUserId
        {
            get { return Convert.ToString(ConfigurationManager.AppSettings["adminapiuserid"]); }
        }
        public static string AdminAPIPassword
        {
            get { return Convert.ToString(ConfigurationManager.AppSettings["adminapipassword"]); }
        }
        public static string AdminAPIAgencyId
        {
            get { return Convert.ToString(ConfigurationManager.AppSettings["adminapiagencyid"]); }
        }
        public static string SeeInsuredConString
        {
            get { return ConfigurationManager.ConnectionStrings["SeeInsured"].ConnectionString; }
        }
        public static string SeeInsuredApiConString
        {
            get { return ConfigurationManager.ConnectionStrings["SeeInsuredApi"].ConnectionString; }
        }

        public static string SeeInsuredMotorConString
        {
            get { return ConfigurationManager.ConnectionStrings["SeeInsuredMotor"].ConnectionString; }
        }
    }
}
