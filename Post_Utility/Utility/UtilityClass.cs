﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Post_Utility.Utility
{
    public static class UtilityClass
    {
        public static string GenrateEnquiryId(int sizelimit, string charcombo)
        {
            DateTime dtCurr = DateTime.Now;
            string todatdate = dtCurr.Year.ToString() + dtCurr.Month.ToString() + dtCurr.Day.ToString();
            return GenrateRandomTransactionId(todatdate, sizelimit, charcombo);
        }
        public static string GetFullPathWithAppData(string path, int agencyID)
        {
            string fullPath = "/App_Data/";

            if (!string.IsNullOrEmpty(path))
            {
                fullPath = fullPath + path + "/" + agencyID + "/";
            }

            return fullPath.Replace("//", "/");
        }
        public static string GetFullPathStringWithAppData(string path, string folderName)
        {
            string fullPath = "/App_Data/";

            if (!string.IsNullOrEmpty(path))
            {
                fullPath = fullPath + path + "/" + folderName + "/";
            }

            return fullPath.Replace("//", "/");
        }
        public static string GetImagePath(string imageName, string folderName, int id)
        {
            return !string.IsNullOrEmpty(imageName) ? "/GetFile.ashx?path=" + folderName + "/" + id + "/" + imageName : string.Empty;
        }
        public static string GetImagePath(string imageName, string folderName, string innerfoldername)
        {
            return !string.IsNullOrEmpty(imageName) ? "/GetFile.ashx?path=" + folderName + "/" + innerfoldername + "/" + imageName : string.Empty;
        }
        public static string GenrateRandomTransactionId(string prefixString, int SizeLimit, string charCombo = null)
        {
            try
            {
                char[] chars = !string.IsNullOrEmpty(charCombo) ? charCombo.ToCharArray() : "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".ToCharArray();
                byte[] data = new byte[SizeLimit];
                using (RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider())
                {
                    crypto.GetBytes(data);
                }

                StringBuilder result = new StringBuilder(SizeLimit);

                if (!string.IsNullOrWhiteSpace(prefixString))
                {
                    result.Append(prefixString);
                }

                foreach (byte b in data)
                {
                    result.Append(chars[b % (chars.Length)]);
                }

                return result.ToString();
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return string.Empty;
        }
        public static string GetDescription(string description, int? length = null)
        {
            string publicDescription = string.Empty;

            if (!string.IsNullOrEmpty(description))
            {
                publicDescription = description;

                publicDescription = publicDescription.Replace("\r\n", "<br/>").Replace("\n", "<br/>").Replace("amp;", "  ");

                if (length != null && publicDescription.Length > length)
                {
                    publicDescription = publicDescription.Remove((int)length);

                    if (publicDescription.LastIndexOf(" ") > 0)
                    {
                        publicDescription = publicDescription.Remove(publicDescription.LastIndexOf(" "));
                    }
                }
            }

            return publicDescription;
        }
        public static string SetSpaceAfterLength(string description, int? length = null)
        {
            string publicDescription = string.Empty;

            if (!string.IsNullOrEmpty(description))
            {
                description = description.Replace("\r\n", "<br/>").Replace("\n", "<br/>").Replace("amp;", "  ");

                if (length != null && description.Length > length)
                {
                    int loopcount = 0;
                    foreach (char c in description)
                    {
                        loopcount = loopcount + 1;
                        if (loopcount >= length)
                        {
                            loopcount = 0;
                            publicDescription = publicDescription + c + " ";
                        }
                        else
                        {
                            publicDescription = publicDescription + c;
                        }
                    }
                }
            }

            return publicDescription;
        }
        public static string FormatURL(string value)
        {
            return value.Replace("//", "/").Replace(":/", "://").ToLower().Trim();
        }
        public static string ConvertDateToISOFormate(string date)
        {
            string rutDate = date;
            if (!string.IsNullOrEmpty(date))
            {
                string[] splitDate = date.Split('/');
                rutDate = splitDate[2] + "-" + splitDate[1] + "-" + splitDate[0];

                DateTime dtCDate = Convert.ToDateTime(rutDate);
                rutDate = dtCDate.ToUniversalTime().ToString("yyyy-MM-ddTHH:mm:ss.sssZ");
            }
            return rutDate;
        }
        public static string IndianMoneyFormat(string fare)
        {
            decimal parsed = decimal.Parse((Math.Ceiling(Convert.ToDecimal(fare)).ToString()), CultureInfo.InvariantCulture);
            CultureInfo hindi = new CultureInfo("hi-IN");
            return string.Format(hindi, "{0:c}", parsed).Replace("₹", "₹ ");
        }
        #region [Encryption/Decryptation]
        public static string DecryptString(string encrString)
        {
            byte[] b;
            string decrypted;
            try
            {
                encrString = Decipher(encrString.Replace('$', '='), 23);
                b = Convert.FromBase64String(encrString);
                decrypted = System.Text.ASCIIEncoding.ASCII.GetString(b);
            }
            catch (FormatException fe)
            {
                decrypted = "";
            }
            return decrypted;
        }
        public static string EnryptString(string strEncrypted)
        {
            byte[] b = System.Text.ASCIIEncoding.ASCII.GetBytes(strEncrypted);
            string encrypted = Convert.ToBase64String(b);
            return Encipher(encrypted.Replace('=','$'), 23);
        }
        internal static string Encipher(string input, int key)
        {
            string output = string.Empty;

            foreach (char ch in input)
                output += cipher(ch, key);

            return output;
        }
        internal static char cipher(char ch, int key)
        {
            if (!char.IsLetter(ch))
            {

                return ch;
            }

            char d = char.IsUpper(ch) ? 'A' : 'a';
            return (char)((((ch + key) - d) % 26) + d);


        }
        internal static string Decipher(string input, int key)
        {
            return Encipher(input, 26 - key);
        }
        #endregion
        //public static string ReadHTMLTemplate(string fileName)
        //{
        //    using (StreamReader streamReader = new StreamReader(HttpContext.Current.Server.MapPath("~") + "\\" + "/EmailTemplate" + "/" + fileName))
        //    {
        //        try
        //        {
        //            string xmlString = streamReader.ReadToEnd();
        //            return xmlString;
        //        }
        //        finally
        //        {
        //            streamReader.Close();
        //            streamReader.Dispose();
        //        }
        //    }
        //}
    }
}
