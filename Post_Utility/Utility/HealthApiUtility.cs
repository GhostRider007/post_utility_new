﻿using Newtonsoft.Json;
using Post_Utility.Health;
using Post_Utility.Model;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Post_Utility.Utility
{
    public static class HealthApiUtility
    {
        private static string GetCommonUrl(string url)
        {
            return "http://api.seeinsured.com/api/" + url;
        }
        private static string GenrateTrackId()
        {
            return UtilityClass.GenrateRandomTransactionId("INS", 7, null);
        }
        public static string GetrAuthenticateToken(string username, string password, string agencyId, string postUrl)
        {
            string trackId = UtilityClass.GenrateRandomTransactionId("INS", 7, null);
            string url = GetCommonUrl(postUrl);
            string requestJson = "{\"username\": \"" + username + "\",\"password\": \"" + password + "\"}";
            return PostHealthAPI("POST", url, requestJson, "Get_Token", agencyId, trackId);
        }
        public static string PostHealthAPI(string postType, string postUrl, string requestJson, string actionType, string agencyId, string trackId, string tokenKey = null)
        {
            string ReturnValue = string.Empty;
            //ConnectToDataBase.Logger("Information", postUrl, "Request captured in post Method : " + postUrl + "", requestJson, actionType, "Health", agencyId, trackId, "");

            StringBuilder sbResult = new StringBuilder();

            //ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;
            HttpWebRequest Http = (HttpWebRequest)WebRequest.Create(postUrl);

            try
            {
                Http.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip,deflate");

                Http.Method = postType;
                byte[] lbPostBuffer = Encoding.UTF8.GetBytes(requestJson);
                if (!string.IsNullOrEmpty(requestJson))
                {
                    Http.ContentLength = lbPostBuffer.Length;
                }
                Http.Timeout = 130000; //5000 milliseconds == 5 seconds// 900000 milliseconds == 900 seconds- 15 mints
                Http.ContentType = "application/json";
                Http.Accept = "application/json";
                if (!string.IsNullOrEmpty(tokenKey))
                {
                    Http.Headers["Authorization"] = "Bearer " + tokenKey;
                }

                if (!string.IsNullOrEmpty(requestJson))
                {
                    using (Stream PostStream = Http.GetRequestStream())
                    {
                        PostStream.Write(lbPostBuffer, 0, lbPostBuffer.Length);
                    }
                }

                using (HttpWebResponse WebResponse = (HttpWebResponse)Http.GetResponse())
                {
                    if (WebResponse.StatusCode != HttpStatusCode.OK)
                    {
                        string message = String.Format("POST failed. Received HTTP {0}", WebResponse.StatusCode);
                        throw new ApplicationException(message);
                    }
                    else
                    {
                        Stream responseStream = WebResponse.GetResponseStream();
                        if ((WebResponse.ContentEncoding.ToLower().Contains("gzip")))
                        {
                            responseStream = new GZipStream(responseStream, CompressionMode.Decompress);
                        }
                        else if ((WebResponse.ContentEncoding.ToLower().Contains("deflate")))
                        {
                            responseStream = new DeflateStream(responseStream, CompressionMode.Decompress);
                        }
                        StreamReader reader = new StreamReader(responseStream, Encoding.Default);
                        sbResult.Append(reader.ReadToEnd());
                        ReturnValue = sbResult.ToString();
                        responseStream.Close();

                        //ConnectToDataBase.Logger("Success", postUrl, "Request captured in post Method : " + postUrl + "", requestJson, ReturnValue, actionType, agencyId, trackId, "");
                    }
                }
            }
            catch (WebException wex)
            {
                HttpWebResponse httpResponse = wex.Response as HttpWebResponse;
                using (Stream responseStream = httpResponse.GetResponseStream())
                {
                    using (StreamReader reader = new StreamReader(responseStream))
                    {
                        ReturnValue = GetWebResponseString(httpResponse);
                    }
                }

                //ConnectToDataBase.Logger("Error", postUrl, "Error : Request captured in post Method : " + postUrl + "", requestJson, "", actionType, agencyId, trackId, httpResponse.StatusDescription);
                //ReturnValue = null;
            }
            finally
            {
                Http.Abort();
                Http = null;
            }

            return ReturnValue;
        }
        public static string GetWebResponseString(HttpWebResponse myHttpWebResponse)
        {
            StringBuilder rawResponse = new StringBuilder();
            string aa = "";
            Stream responseStream = myHttpWebResponse.GetResponseStream();
            Stream streamResponse = responseStream;
            using (responseStream)
            {
                if (myHttpWebResponse.ContentEncoding.ToLower().Contains("gzip") || myHttpWebResponse.ContentEncoding.ToLower().Contains("deflate"))
                {
                    if (myHttpWebResponse.ContentEncoding.ToLower().Contains("gzip"))
                    {
                        streamResponse = new GZipStream(streamResponse, CompressionMode.Decompress);
                    }
                    else if (myHttpWebResponse.ContentEncoding.ToLower().Contains("deflate"))
                    {
                        streamResponse = new DeflateStream(streamResponse, CompressionMode.Decompress);
                    }
                    using (StreamReader streamRead = new StreamReader(streamResponse))
                    {
                        //char[] readBuffer = new char[checked((IntPtr)myHttpWebResponse.ContentLength)];

                        //for (int count = streamRead.Read(readBuffer, 0, Convert.ToInt32(myHttpWebResponse.ContentLength)); count > 0; count = streamRead.Read(readBuffer, 0, Convert.ToInt32(myHttpWebResponse.ContentLength)))
                        //{
                        //    rawResponse.Append(new string(readBuffer, 0, count));
                        //}
                    }
                    aa = rawResponse.ToString();
                }
                else
                {
                    aa = (new StreamReader(streamResponse)).ReadToEnd().Trim();
                }
            }
            return aa;
        }
        public static string PostHealtApiWithoutBody(string postUrl, string tokenKey)
        {
            var client = new RestClient(postUrl);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + tokenKey);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }

        #region [Health Request / Response]
        public static string GetHealthSearchListing(string requestJson, string agencyId, string tokenKey)
        {
            return PostHealthAPI("POST", GetCommonUrl("SearchProduct/searchProduct"), requestJson, "Get_Health_Listing", agencyId, GenrateTrackId(), tokenKey);
        }
        public static string CreateHealthProposalResponse(string requestJson, string agencyId, string tokenKey)
        {
            return PostHealthAPI("POST", GetCommonUrl("CreateProposal/CreateProposal"), requestJson, "Get_Health_Perposal", agencyId, GenrateTrackId(), tokenKey);
        }
        public static string GenrateHealthPaymentUrl(string enquiryid, string raw_paymenturl, string tokenKey)
        {
            return PostHealtApiWithoutBody(raw_paymenturl, tokenKey);
        }
        public static string GetProposalAndPolicy(string enquiryid, string referenceId, string tokenKey, string companyid)
        {
            string response = string.Empty;
            if (!string.IsNullOrEmpty(referenceId))
            {
                if (companyid == "2")
                {
                    string resqUrl = GetCommonUrl("Care/PolicyStatus/" + referenceId + "/" + enquiryid);
                    response = PostHealtApiWithoutBody(resqUrl, tokenKey);
                }
                else
                {
                    string resqUrl = GetCommonUrl("StarHealth/GetProposalAndPolicy/" + referenceId + "/" + enquiryid);
                    response = PostHealtApiWithoutBody(resqUrl, tokenKey);
                }
            }
            return response;
        }
        public static string Genrate_HealthPdfDocument(string enquiryid, string tokenKey, string p_referenceId, string companyid, ref string pdfdocurl)
        {
            try
            {
                if (companyid == "2")
                {
                    pdfdocurl = GetCommonUrl("Care/PolicyDocument/" + p_referenceId + "/" + enquiryid + "/POLSCHD");
                    return PostHealtApiWithoutBody(pdfdocurl, tokenKey);
                }
                else if (companyid == "4")
                {
                }
                else
                {
                    return PostHealtApiWithoutBody(GetCommonUrl("StarHealth/PolicyDocument/" + p_referenceId + "/" + enquiryid), tokenKey);
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return string.Empty;
        }
        public static string GetHealthQuestionariesList(string strRequest, string tokenKey)
        {
            return PostHealtApiWithoutBody(strRequest, tokenKey);
        }
        public static string GetQuestionariesUrl(string companyid, string productid)
        {
            return GetCommonUrl("Questionnaire/Questionnaire/" + companyid + "/" + productid);
        }
        public static string GetCityListFromStarHealth(string tokenKey, string pincode)
        {
            try
            {
                string strRequest = GetCommonUrl("StarHealth/GetCity?pincode=" + pincode);

                return PostHealtApiWithoutBody(strRequest, tokenKey);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        public static string GetAreaListFromStarHealth(string tokenKey, string pincode, string cityid)
        {
            try
            {
                string strRequest = GetCommonUrl("StarHealth/GetArea?pincode=" + pincode + "&city_id=" + cityid);

                return PostHealtApiWithoutBody(strRequest, tokenKey);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        public static string GetCompareInsuranceProducts(int companyid, string productid, string plan, int sumInsured, string agencyId, string tokenKey)
        {
            string requestJson = "{\"companyId\":" + companyid + ", \"productid\":\"" + productid + "\", \"plan\": \"" + plan + "\", \"sumInsured\":" + sumInsured + " }";
            return PostHealthAPI("POST", GetCommonUrl("Benefits/GetBenefits"), requestJson, "GetBenefits", agencyId, GenrateTrackId(), tokenKey);
        }
        #endregion

        #region [Adity Birla Save_Perposal]
        public static string AdityBirla_Save_Perposal(string productid, string companyid, string enquiryid, string agencyId, string tokenKey)
        {
            string response = string.Empty;
            try
            {
                string reqJson = "{\"insuranceCompanyId\":" + companyid + ",\"inquiry_id\":\"" + enquiryid + "\",\"id\":0,\"productDetailId\":\"" + productid + "\"}";
                if (Post_Health.UpdateAdityBirlaSaveProposal(reqJson, enquiryid, "request"))
                {
                    response = PostHealthAPI("POST", GetCommonUrl("CreateProposal/SaveProposal"), reqJson, "SaveProposal", agencyId, GenrateTrackId(), tokenKey);
                    bool issuccess = Post_Health.UpdateAdityBirlaSaveProposal(response, enquiryid, "response");
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return response;
        }
        #endregion
    }
}
