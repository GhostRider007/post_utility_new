﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Post_Utility.Utility
{
    public static class SmsUtility
    {
        public static bool Send_SeeinsuredSMS(string mobile, string message)
        {
            bool issent = false;
            if (!string.IsNullOrEmpty(mobile.Trim()))
            {
                int mob_length = mobile.Length;
                if (mob_length == 11)
                {
                    mobile = mobile.Substring(1);
                }
                else if (mob_length == 12)
                {
                    mobile = mobile.Substring(2);
                }
                string sms_query = "http://mobicomm.dove-sms.com//submitsms.jsp?user=SSTrvlPL&key=70a9327a28XX&mobile=+91" + mobile + "&message=" + message + "&senderid=SEEINS&accusage=1";
                var client = new RestClient(sms_query);
                client.Timeout = -1;
                var request = new RestRequest(Method.POST);
                request.AddParameter("text/plain", "", ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                if (!string.IsNullOrEmpty(response.ToString()))
                {
                    string respo = response.Content.Replace("\r\n", string.Empty).Replace("\n\n", string.Empty).Trim();
                    string[] spilit_respo = respo.Split(',');
                    if (spilit_respo[0] == "sent" && spilit_respo[1] == "success")
                    {
                        issent = true;
                    }
                }
            }

            return issent;
        }
    }
}
