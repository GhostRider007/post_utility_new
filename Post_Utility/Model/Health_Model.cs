﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Post_Utility.Model
{
    public class PaymentSuccessDel
    {
        public string csrf { get; set; }
        public string policyNumber { get; set; }
        public string transactionRefNum { get; set; }
        public string uwDecision { get; set; }
        public string errorFlag { get; set; }
        public string errorMsg { get; set; }
        public string enquiryid { get; set; }
        public string companyName { get; set; }
        public string Status { get; set; }
        public string imgurl { get; set; }
        public string companyid { get; set; }
        public string premium { get; set; }
        public string proposalNum { get; set; }
        public string purchaseToken { get; set; }
        //-----------------Adity Birla---------------------------------
        public string merchantTxnId { get; set; }
        public string amount { get; set; }
        public string currency { get; set; }
        public string email { get; set; }
        public string mobileNo { get; set; }
        public string paymentMode { get; set; }
        public string pgRespCode { get; set; }
        public string QuoteId { get; set; }
        public string SourceTxnId { get; set; }
        public string TxMsg { get; set; }
        public string TxStatus { get; set; }
        public string TxRefNo { get; set; }
        public string txnDateTime { get; set; }
        public string signature { get; set; }
    }

    #region [Adity Birla Save_Perposal]
    public class ABHIData
    {
        public string referenceId { get; set; }
        public string premium { get; set; }
        public string serviceTax { get; set; }
        public string totalPremium { get; set; }
        public string proposalNum { get; set; }
        public string productDetailId { get; set; }
        public int companyId { get; set; }
        public string inquiry_id { get; set; }
        public int id { get; set; }
        public string receiptAmount { get; set; }
        public string receiptNumber { get; set; }
        public string policyNumber { get; set; }
        public string policyStatus { get; set; }
    }
    public class ABHIResponse
    {
        public ABHIData data { get; set; }
        public object urls { get; set; }
        public string message { get; set; }
    }
    public class ABHISaveProposal
    {
        public bool success { get; set; }
        public string message { get; set; }
        public ABHIResponse response { get; set; }
        public int error_code { get; set; }
    }
    #endregion
}
