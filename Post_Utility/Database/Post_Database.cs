﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Post_Utility.Database
{
    public static class Post_Database
    {
        private static SqlConnection SeeInsuredConString = new SqlConnection(Post_ConfigFile.SeeInsuredConString);
        private static SqlConnection SeeInsuredApiConString = new SqlConnection(Post_ConfigFile.SeeInsuredApiConString);
        private static SqlConnection SeeInsuredMotorConString = new SqlConnection(Post_ConfigFile.SeeInsuredMotorConString);
        private static SqlCommand Command { get; set; }
        private static SqlDataAdapter Adapter { get; set; }
        private static DataSet ObjDataSet { get; set; }
        private static DataTable ObjDataTable { get; set; }
        private static void OpenConnection(SqlConnection constr)
        {
            if (constr.State == ConnectionState.Closed)
            {
                constr.Open();
            }
        }
        public static void CloseConnection(SqlConnection constr)
        {
            if (constr.State == ConnectionState.Open)
            {
                constr.Close();
            }
        }
        private static SqlConnection GetConnectionString(string dbType)
        {
            if (dbType == "api")
            {
                return SeeInsuredApiConString;
            }
            else if (dbType == "motor")
            {
                return SeeInsuredMotorConString;
            }
            return SeeInsuredConString;
        }
        public static DataTable GetRecordFromTable(string fileds, string tablename, string whereCondition, string database)
        {
            try
            {
                Command = new SqlCommand("usp_GetRecordFromTable", GetConnectionString(database));
                Command.CommandType = CommandType.StoredProcedure;
                Command.Parameters.AddWithValue("fileds", fileds);
                Command.Parameters.AddWithValue("tablename", tablename);
                Command.Parameters.AddWithValue("where", whereCondition);

                Adapter = new SqlDataAdapter();
                Adapter.SelectCommand = Command;
                ObjDataSet = new DataSet();

                ObjDataTable = new DataTable();
                Adapter.Fill(ObjDataSet, "CommonTable");
                ObjDataTable = ObjDataSet.Tables["CommonTable"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return ObjDataTable;
        }
        public static int InsertRecordToTableReturnID(string fileds, string fieldValue, string tableName, string database)
        {
            try
            {
                Command = new SqlCommand("usp_InsertRecordToTableReturnID", GetConnectionString(database));
                Command.CommandType = CommandType.StoredProcedure;
                Command.Parameters.Add(new SqlParameter("@fileds", fileds));
                Command.Parameters.Add(new SqlParameter("@values", fieldValue));
                Command.Parameters.Add(new SqlParameter("@tablename", tableName));
                Command.Parameters.Add("@Id", SqlDbType.Int).Direction = ParameterDirection.Output;

                OpenConnection(GetConnectionString(database));
                int isSuccess = Command.ExecuteNonQuery();
                string id = Command.Parameters["@Id"].Value.ToString();
                CloseConnection(GetConnectionString(database));

                if (isSuccess > 0)
                {
                    return Convert.ToInt32(id);
                }

            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return 0;
        }
        public static bool UpdateRecordIntoAnyTable(string tableName, string fieldsWithValue, string whereCondition, string database)
        {
            try
            {
                Command = new SqlCommand("ups_UpdateRecordToTable", GetConnectionString(database));
                Command.CommandType = CommandType.StoredProcedure;
                Command.Parameters.AddWithValue("TableName", tableName);
                Command.Parameters.AddWithValue("FieldsWithValue", fieldsWithValue);
                Command.Parameters.AddWithValue("WhereCondition", whereCondition);

                OpenConnection(GetConnectionString(database));
                int isSuccess = Command.ExecuteNonQuery();
                CloseConnection(GetConnectionString(database));

                if (isSuccess > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }
        public static bool DeleteRecordFromAnyTable(string tableName, string whereCondition, string database)
        {
            try
            {
                Command = new SqlCommand("usp_DeleteRecordFromTable", GetConnectionString(database));
                Command.CommandType = CommandType.StoredProcedure;
                Command.Parameters.AddWithValue("tablename", tableName);
                Command.Parameters.AddWithValue("where", whereCondition);

                OpenConnection(GetConnectionString(database));
                int isSuccess = Command.ExecuteNonQuery();
                CloseConnection(GetConnectionString(database));

                if (isSuccess > 0)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }
        public static bool InsertRecordToTable(string fileds, string fieldValue, string tableName, string database)
        {
            try
            {
                Command = new SqlCommand("usp_InsertRecordToTable", GetConnectionString(database));
                Command.CommandType = CommandType.StoredProcedure;
                Command.Parameters.AddWithValue("fileds", fileds);
                Command.Parameters.AddWithValue("values", fieldValue);
                Command.Parameters.AddWithValue("tablename", tableName);

                OpenConnection(GetConnectionString(database));
                int isSuccess = Command.ExecuteNonQuery();
                CloseConnection(GetConnectionString(database));

                if (isSuccess > 0)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }
        public static DataTable GetCommisionLedgerListForReport(string agencyId, string month, string year, string database)
        {
            try
            {
                Command = new SqlCommand("SP_GetIncentiveCommission", GetConnectionString(database));
                Command.CommandType = CommandType.StoredProcedure;
                Command.Parameters.AddWithValue("AgencyId", agencyId);
                Command.Parameters.AddWithValue("month", month);
                Command.Parameters.AddWithValue("year", year);

                Adapter = new SqlDataAdapter();
                Adapter.SelectCommand = Command;
                ObjDataSet = new DataSet();

                ObjDataTable = new DataTable();
                Adapter.Fill(ObjDataSet, "CommonTable");
                ObjDataTable = ObjDataSet.Tables["CommonTable"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return ObjDataTable;
        }
        public static DataTable IncCommGroupAgencyDetail(string type)
        {
            try
            {
                string database = "seeinsured";
                Command = new SqlCommand("sp_IncCommGroupAgencyDetail", GetConnectionString(database));
                Command.CommandType = CommandType.StoredProcedure;
                Command.Parameters.AddWithValue("type", type);

                Adapter = new SqlDataAdapter();
                Adapter.SelectCommand = Command;
                ObjDataSet = new DataSet();

                ObjDataTable = new DataTable();
                Adapter.Fill(ObjDataSet, "CommonTable");
                ObjDataTable = ObjDataSet.Tables["CommonTable"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return ObjDataTable;
        }
        public static DataSet GetProposalEmailDetail(string enquiryid)
        {
            try
            {
                string database = "api";
                Command = new SqlCommand("sp_ProposalEmailSendDetail", GetConnectionString(database));
                Command.CommandType = CommandType.StoredProcedure;
                Command.Parameters.AddWithValue("enquiryid", enquiryid);

                Adapter = new SqlDataAdapter();
                Adapter.SelectCommand = Command;
                ObjDataSet = new DataSet();

                ObjDataTable = new DataTable();
                Adapter.Fill(ObjDataSet, "CommonTable");
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return ObjDataSet;
        }
        public static bool IncentiveCopy(string fromagencyid, string fromgroupid, string toagencyid, string toagencyname, string togroupid, string togroupname)
        {
            try
            {
                string database = "seeinsured";
                Command = new SqlCommand("sp_IncentiveCopy", GetConnectionString(database));
                Command.CommandType = CommandType.StoredProcedure;
                Command.Parameters.AddWithValue("FromGroupId", fromgroupid);
                Command.Parameters.AddWithValue("FromAgencyId", fromagencyid);
                Command.Parameters.AddWithValue("GroupId", togroupid);
                Command.Parameters.AddWithValue("GroupName", togroupname);
                Command.Parameters.AddWithValue("AgencyId", toagencyid);
                Command.Parameters.AddWithValue("AgencyName", toagencyname);

                OpenConnection(GetConnectionString(database));
                int isSuccess = Command.ExecuteNonQuery();
                CloseConnection(GetConnectionString(database));

                if (isSuccess > 0)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }
        public static bool UpdateCommission_PassBook(string enquiryId, string amount, string database)
        {
            try
            {
                Command = new SqlCommand("sp_InsertHealthIRDAICommission", GetConnectionString(database));
                Command.CommandType = CommandType.StoredProcedure;
                Command.Parameters.AddWithValue("enquiryId", enquiryId);
                Command.Parameters.AddWithValue("amount", amount);

                OpenConnection(GetConnectionString(database));
                int isSuccess = Command.ExecuteNonQuery();
                CloseConnection(GetConnectionString(database));

                if (isSuccess > 0)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }
    }
}
