﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Post_Utility.Database;

namespace Post_Utility.Admin_Backend
{
    public static class AdminBackend_CommissionHelper
    {
        public static DataTable GetCommissionList(string commisionId = "", string status = "")
        {
            string whereCondition = string.Empty;

            if (!string.IsNullOrEmpty(status))
            {
                whereCondition = "Status=" + status;
            }

            if (!string.IsNullOrEmpty(commisionId))
            {
                whereCondition += !string.IsNullOrEmpty(whereCondition) ? " and CommisionId=" + commisionId : "CommisionId=" + commisionId;
            }

            string fileds = "*";
            string tablename = "T_Commision";
            return Post_Database.GetRecordFromTable(fileds, tablename, whereCondition, "seeinsured");
        }

        public static bool InsertCommissionDetail(string basicPer, string basicTDS, string incentivePer, string incentiveTDS)
        {
            string fileds = "BasicPer,BasicTDS,IncentivePer,IncentiveTDS";
            string fieldValue = "'" + basicPer + "','" + basicTDS + "','" + incentivePer + "','" + incentiveTDS + "'";
            string tableName = "T_Commision";
            return Post_Database.InsertRecordToTable(fileds, fieldValue, tableName, "seeinsured");
        }
        public static bool UpdateCommissionStatus(string comissionid, string status)
        {
            string fieldsWithValue = "Status=" + status;
            string tableName = "T_Commision";
            string whereCondition = "CommisionId=" + comissionid;
            return Post_Database.UpdateRecordIntoAnyTable(tableName, fieldsWithValue, whereCondition, "seeinsured");
        }
        public static bool UpdateCommissionDetail(string basicPer, string basicTDS, string incentivePer, string incentiveTDS, int comissionid)
        {
            string fieldsWithValue = "BasicPer='" + basicPer + "',BasicTDS='" + basicTDS + "',IncentivePer='" + incentivePer + "',IncentiveTDS='" + incentiveTDS + "'";
            string tableName = "T_Commision";
            string whereCondition = "CommisionId=" + comissionid;
            return Post_Database.UpdateRecordIntoAnyTable(tableName, fieldsWithValue, whereCondition, "seeinsured");
        }

        //====================================NEW ONES========================================================

        public static DataTable CommissionList(int? id, string insuramceType, string status = null)
        {
            string whereCondition = string.Empty;

            if (!string.IsNullOrEmpty(status))
            {
                whereCondition = "Status=" + status;
            }

            if (id > 0)
            {
                whereCondition += !string.IsNullOrEmpty(whereCondition) ? " and id=" + id : "id=" + id;
            }

            string fileds = "*";
            string tablename = "tbl_Commission";
            return Post_Database.GetRecordFromTable(fileds, tablename, whereCondition, "seeinsured");
        }
        public static bool InsertCommission(Dictionary<string, object> dObj, Dictionary<string, string> dObj_Str)
        {
            string fileds = string.Empty;
            string fieldValue = string.Empty;

            foreach (var item in dObj)
            {
                fileds = fileds + item.Key + ",";
                fieldValue = fieldValue + item.Value + ",";
            }
            foreach (var item in dObj_Str)
            {
                fileds = fileds + item.Key + ",";
                fieldValue = fieldValue + "'" + item.Value + "'" + ",";
            }
            fileds = fileds.Remove(fileds.Length - 1);
            fieldValue = fieldValue.Remove(fieldValue.Length - 1);

            return Post_Database.InsertRecordToTable(fileds, fieldValue, "tbl_Commission", "seeinsured");
        }
        public static bool UpdateCommission(Dictionary<string, object> dObj, Dictionary<string, string> dObj_Str)
        {
            string fieldsWithValue = string.Empty;
            string whereCondition = "id = " + dObj["id"];

            foreach (var item in dObj)
            {
                fieldsWithValue = fieldsWithValue + item.Key + "=" + item.Value + ",";
            }
            foreach (var item in dObj_Str)
            {
                fieldsWithValue = fieldsWithValue + item.Key + "='" + item.Value + "',";
            }
            fieldsWithValue = fieldsWithValue.Remove(fieldsWithValue.Length - 1);
            fieldsWithValue = fieldsWithValue.Replace("id=" + dObj["id"] + ",", "");

            return Post_Database.UpdateRecordIntoAnyTable("tbl_Commission", fieldsWithValue, whereCondition, "seeinsured");
        }
        public static bool CheckIfRecordExistCommission(Dictionary<string, object> dObj, Dictionary<string, string> dObj_Str)
        {
            string whereCondition = "";

            foreach (var item in dObj)
            {
                whereCondition = whereCondition + item.Key + "=" + item.Value + " and ";
            }
            foreach (var item in dObj_Str)
            {
                whereCondition = whereCondition + item.Key + "='" + item.Value + "' and ";
            }
            whereCondition = whereCondition.Remove(whereCondition.Length - 4);

            DataTable dt = Post_Database.GetRecordFromTable("Count(*) as Count", "tbl_Commission", whereCondition, "seeinsured");
            int count = Convert.ToInt32(dt.Rows[0]["Count"].ToString());
            if (count > 0)
            {
                return true;
            }
            return false;
        }
        public static bool CheckIfRecordExistInsentiveCommission(Dictionary<string, string> dObj)
        {
            string whereCondition = "";

            foreach (var item in dObj)
            {
                whereCondition = whereCondition + item.Key + "='" + item.Value + "' and ";
            }
            whereCondition = whereCondition.Remove(whereCondition.Length - 4);

            DataTable dt = Post_Database.GetRecordFromTable("Count(*) as Count", "T_IncentiveCommission", whereCondition, "seeinsured");
            int count = Convert.ToInt32(dt.Rows[0]["Count"].ToString());
            if (count > 0)
            {
                return true;
            }
            return false;
        }
        public static bool DeleteCommission(string id)
        {
            return Post_Database.UpdateRecordIntoAnyTable("tbl_Commission", "Status=0", "id = " + id, "seeinsured");
        }
        public static DataTable GetCompanyList(string type)
        {
            DataTable dt = new DataTable();
            string fileds = string.Empty;

            if (type == "health")
            {
                fileds = "CompanyId,CompanyName";
                dt = Post_Database.GetRecordFromTable(fileds, "tbl_Company", "Status=1", "api");
            }
            if (type == "motor")
            {
                fileds = "CompanyId as CompanyId, Supplier as CompanyName";
                dt = Post_Database.GetRecordFromTable(fileds, "T_MotorCredential", "Active=1", "api");
            }
            return dt;
        }
        public static DataTable GetProductList(string type, string CompanyId, string vehicleType = null)
        {
            DataTable dt = new DataTable();
            string fileds = string.Empty;

            if (type.ToLower() == "health")
            {
                fileds = "ProductId as ProductId,ProductName as ProductName";
                dt = Post_Database.GetRecordFromTable(fileds, "tbl_Product", "CompanyId = " + CompanyId + " and Active = 1", "api");
            }
            if (type.ToLower() == "motor")
            {
                fileds = "id as ProductId,Concat(ProductName,' - ',PolicyType) as ProductName";
                dt = Post_Database.GetRecordFromTable(fileds, "tblMaster_ProductDetails", "CompanyId = " + CompanyId + " and ProductType = '" + vehicleType + "' and Status=1", "api");
            }

            return dt;
        }
        public static bool AddEditInsentiveComission(int id, Dictionary<string, string> incentive)
        {
            bool issuccess = false;
            try
            {
                if (id > 0)
                {
                    string fieldsWithValue = string.Empty;
                    string whereCondition = "id = " + id;

                    foreach (var item in incentive)
                    {
                        fieldsWithValue = fieldsWithValue + item.Key + "='" + item.Value + "',";
                    }
                    fieldsWithValue = fieldsWithValue.Remove(fieldsWithValue.Length - 1);

                    issuccess = Post_Database.UpdateRecordIntoAnyTable("T_IncentiveCommission", fieldsWithValue, whereCondition, "seeinsured");
                }
                else
                {
                    string fileds = string.Empty;
                    string fieldValue = string.Empty;

                    foreach (var item in incentive)
                    {
                        fileds = fileds + item.Key + ",";
                        fieldValue = fieldValue + "'" + item.Value + "'" + ",";
                    }
                    fileds = fileds.Remove(fileds.Length - 1);
                    fieldValue = fieldValue.Remove(fieldValue.Length - 1);
                    issuccess = Post_Database.InsertRecordToTable(fileds, fieldValue, "T_IncentiveCommission", "seeinsured");
                    if (issuccess)
                    {
                        fileds = string.Empty;
                        fieldValue = string.Empty;
                        foreach (var item in incentive)
                        {
                            if (item.Key.Contains("GroupId") || item.Key.Contains("GroupName") || item.Key.Contains("AgencyId") || item.Key.Contains("AgencyName") || item.Key.Contains("InsurerId") || item.Key.Contains("InsurerName") || item.Key.Contains("ProductCode") || item.Key.Contains("ProductName") || item.Key.Contains("IRDAI") || item.Key.Contains("IRDAI_TDS") || item.Key.Contains("FromDate") || item.Key.Contains("ToDate"))
                            {
                                fileds = fileds + item.Key + ",";
                                fieldValue = fieldValue + "'" + item.Value + "'" + ",";
                            }
                        }
                        fileds = fileds + "InsuranceType,CommissionType";
                        fieldValue = fieldValue + "'health','percentage'";
                        issuccess = Post_Database.InsertRecordToTable(fileds, fieldValue, "tbl_commission", "seeinsured");
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return issuccess;
        }
        public static DataTable GetHealthInsentiveComissionList(string status, string id = null, string FromDate = null, string ToDate = null)
        {
            string whrcon = "Status=" + status;
            if (!string.IsNullOrEmpty(id))
            {
                whrcon = whrcon + " and id=" + id;
            }
            if (!string.IsNullOrEmpty(FromDate))
            {
                FromDate = Convert.ToDateTime(FromDate).ToString("yyyy-MM-dd HH:mm:ss");
                whrcon = whrcon + " and FromDate >= '" + FromDate + "'";
                
            }
            if (!string.IsNullOrEmpty(ToDate))
            {
                ToDate = Convert.ToDateTime(ToDate).AddDays(1).AddSeconds(-1).ToString("yyyy-MM-dd HH:mm:ss");
                whrcon = whrcon + " and ToDate <='" + ToDate + "'";
            }
            return Post_Database.GetRecordFromTable("*", "T_IncentiveCommission", whrcon, "seeinsured");
        }
        public static bool UpdateInsentiveComission(string id, string status)
        {
            return Post_Database.UpdateRecordIntoAnyTable("T_IncentiveCommission", "Status=" + status, "id=" + id, "seeinsured");
        }
        public static DataTable GetCommisionLedgerListForReport(string agencyId, string time, string insuranceType)
        {
            string year = time.Split('-')[0];
            string month = time.Split('-')[1];
            agencyId = agencyId != "0" ? agencyId : "";

            //string fields_Ledger = "h.AgencyName,p.row_insuranceCompany as Insurer,(select ProductName from tbl_Product where productid=p.productid) as ProductName,sum(CONVERT(float, p.row_totalPremium))AS TotalSale,(select GroupId from[seeinsuredproduct_test].[dbo].[T_GroupDetail] where GroupId in (select GroupType from[seeinsuredapi_test].[dbo].[aspnet_user] where AgencyId = h.AgencyId)) as GroupId,(select GroupName from[seeinsuredproduct_test].[dbo].[T_GroupDetail] where GroupId in (select GroupType from[seeinsuredapi_test].[dbo].[aspnet_user] where AgencyId = h.AgencyId)) as GroupName";
            //string whereCondition_Ledger = "(pay_policy_Number is not null and pay_policy_Number<>'') and MONTH(pay_paymentdate) = '" + month + "' and Year(pay_paymentdate)='" + year + "'";
            //whereCondition_Ledger = whereCondition_Ledger + (!string.IsNullOrEmpty(agencyId) ? " and h.AgencyId='" + agencyId + "'" : string.Empty);
            //whereCondition_Ledger = whereCondition_Ledger + " GROUP BY p.row_insuranceCompany,h.AgencyName,p.productid,h.AgencyId order by h.AgencyName";
            //return Post_Database.GetRecordFromTable(fields_Ledger, "T_NewEnquiry_Health h inner join T_Health_Proposal p on h.Enquiry_Id = p.enquiryid", whereCondition_Ledger, "api");

            return Post_Database.GetCommisionLedgerListForReport(agencyId, month, year, "api");
        }
        public static DataTable IncCommGroupAgencyDetail(string type)
        {
            return Post_Database.IncCommGroupAgencyDetail(type);
        }
        public static bool IncentiveCopy(string fromagencyid, string fromgroupid, string toagencyid, string toagencyname, string togroupid, string togroupname)
        {
            return Post_Database.IncentiveCopy(fromagencyid, fromgroupid, toagencyid, toagencyname, togroupid, togroupname);
        }
    }
}
