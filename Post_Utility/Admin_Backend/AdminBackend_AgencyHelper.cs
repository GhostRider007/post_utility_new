﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Post_Utility.Database;

namespace Post_Utility.Admin_Backend
{
    public static class AdminBackend_AgencyHelper
    {
        public static DataTable GetAgencyList(string agencyId = "", string status = "", string agencyName = "")
        {
            string whereCondition = string.Empty;
            if (!string.IsNullOrEmpty(status))
            {
                whereCondition = "Status=" + status;
            }

            if (!string.IsNullOrEmpty(agencyId))
            {
                whereCondition += !string.IsNullOrEmpty(whereCondition) ? " and AgencyID=" + agencyId : "AgencyID=" + agencyId;
            }

            if (!string.IsNullOrEmpty(agencyName))
            {
                whereCondition += !string.IsNullOrEmpty(whereCondition) ? " and AgencyName='" + agencyName + "'" : "AgencyName='" + agencyName + "'";
            }

            string fileds = "*";
            string tablename = "T_Agency";
            return Post_Database.GetRecordFromTable(fileds, tablename, whereCondition, "seeinsured");
        }
    }
}
