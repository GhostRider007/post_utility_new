﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Post_Utility.Database;

namespace Post_Utility.Admin_Backend
{
    public static class AdminBackend_AccountHelper
    {
        public static DataTable GetLoginMemberByUserId(string userId, string password = "")
        {
            string fileds = "*";
            string tablename = "T_MemberLogin";
            string wherecon = "UserId='" + userId + "'" + (!string.IsNullOrEmpty(password) ? " and Password='" + password + "'" : string.Empty);
            return Post_Database.GetRecordFromTable(fileds, tablename, wherecon, "seeinsured");
        }

        public static DataTable IsUserLoginSuccess(int loginId)
        {
            string fileds = "*";
            string tablename = "T_MemberLogin";
            string wherecon = "LoginId=" + loginId;
            return Post_Database.GetRecordFromTable(fileds, tablename, wherecon, "seeinsured");
        }

        public static DataTable IsUserAlreadyExist(string userId)
        {
            string fileds = "*";
            string tablename = "T_MemberLogin";
            string wherecon = "UserId=" + userId;
            return Post_Database.GetRecordFromTable(fileds, tablename, wherecon, "seeinsured");
        }

        public static bool UpdateMemberImage(string loginId, string imgurl)
        {
            string fieldsWithValue = "Image='" + imgurl + "'";
            string tableName = "T_MemberLogin";
            string whereCondition = "LoginId=" + loginId;
            return Post_Database.UpdateRecordIntoAnyTable(tableName, fieldsWithValue, whereCondition, "seeinsured");
        }

        public static DataTable GetLoginMemberByLoginId(string loginId, string password = "")
        {
            string fileds = "*";
            string tablename = "T_MemberLogin";
            string wherecon = "LoginId='" + loginId + "'" + (!string.IsNullOrEmpty(password) ? " and Password='" + password + "'" : string.Empty);
            return Post_Database.GetRecordFromTable(fileds, tablename, wherecon, "seeinsured");
        }

        public static bool Update_MemberPassword(string password, string loginId)
        {
            string fieldsWithValue = "Password='" + password + "',UpdatedDate=getdate()";
            string tableName = "T_MemberLogin";
            string whereCondition = "LoginId='" + loginId + "'";
            return Post_Database.UpdateRecordIntoAnyTable(tableName, fieldsWithValue, whereCondition, "seeinsured");
        }

        public static bool Admin_ChangeAgencyPassword(string password, string agencyId)
        {
            string fieldsWithValue = "AgencyId='" + agencyId + "'";
            string tableName = "aspnet_user";
            string whereCondition = "Password='" + password + "'";
            return Post_Database.UpdateRecordIntoAnyTable(tableName, fieldsWithValue, whereCondition, "api");
        }

        public static DataTable GetHealth_BookingDetail(string fromDate = "", string toDate = "", string agencyId = "", string insuranceType = "", string insurer = "", string product = "", string CustomerName = "")
        {
            string tablename = "T_NewEnquiry_Health a inner Join T_Health_Proposal b on a.enquiry_id = b.enquiryid  inner join T_Health_Payment c on a.enquiry_id = c.enquiryid inner join T_Enquiry_Response d on a.enquiry_id = d.enquiry_id";

            string fileds = "a.AgencyId as AgencyID, a.AgencyName as AgencyName, a.Enquiry_Id as Enquiry_Id , 'Health' as Insurance,";
            fileds = fileds + "(Select CompanyName from tbl_Company where CompanyId = b.companyid) as Insurer,";
            fileds = fileds + "b.pay_policy_Number as PolicyNo, d.suminsured as SumInsured, CONCAT(b.title,' ',b.firstname,' ',b.lastname) as Proposer_Name,";
            fileds = fileds + "(select top 1 CONCAT(title,' ',firstname,' ',lastname) from T_Health_Insured where enquiryid = a.enquiry_id) as Insured_Name,";
            fileds = fileds + "c.pp_premium as Basic ,c.pp_serviceTax as GST ,c.pp_totalPremium as Premium, 0 as Discount,0 as TDS, ";
            fileds = fileds + "(select JSON_VALUE(perposalrequest,'$.products.policyName') from T_Enquiry_Response where enquiry_id = a.enquiry_id) as ProductName,";
            fileds = fileds + "a.Policy_Type as PolicyType,b.periodfor as Tenure, c.CreatedDate as PolicyDate";

            string whereCondition = "(b.pay_policy_Number is not null and b.pay_policy_Number <> '')";

            if (!string.IsNullOrEmpty(agencyId))
            {
                whereCondition = whereCondition + " and a.AgencyId='" + agencyId + "'";
            }

            if (string.IsNullOrEmpty(fromDate) && string.IsNullOrEmpty(toDate))
            {
                whereCondition = whereCondition + " and  c.CreatedDate > (select CAST(CAST(GETDATE() AS DATE) AS DATETIME))";
            }
            else
            {
                if (!string.IsNullOrEmpty(fromDate))
                {
                    whereCondition = whereCondition + " and  c.CreatedDate > '" + fromDate + "'";
                }

                if (!string.IsNullOrEmpty(toDate))
                {
                    whereCondition = whereCondition + " and  c.CreatedDate <= DATEADD(Day, 1, '" + toDate + "')";
                }
            }

            if (!string.IsNullOrEmpty(CustomerName) && insuranceType.ToLower() == "health")
            {
                whereCondition = whereCondition + " and (CONCAT(b.title,' ',b.firstname,' ',b.lastname) like '%" + CustomerName + "%')";
            }

            if (!string.IsNullOrEmpty(insurer) && insuranceType.ToLower() == "health")
            {
                whereCondition = whereCondition + " and ((Select CompanyName from tbl_Company where CompanyId = b.companyid) = '" + insurer + "' )";
            }

            if (!string.IsNullOrEmpty(product) && insuranceType.ToLower() == "health")
            {
                whereCondition = whereCondition + " and ((select JSON_VALUE(perposalrequest,'$.products.policyName') from T_Enquiry_Response where ISJSON(perposalrequest) = 1 and perposalrequest is not null and enquiry_id = a.enquiry_id) = '" + product + "' )";

            }

            whereCondition = whereCondition + " order by c.CreatedDate desc";

            return Post_Database.GetRecordFromTable(fileds, tablename, whereCondition, "api");
        }

        public static DataTable GetMotor_BookingDetail(string fromDate = "", string toDate = "", string agencyId = "", string insuranceType = "", string insurer = "", string product = "", string CustomerName = "")
        {
            string tablename = "T_NewEnquiry_Motor a inner join T_Motor_Proposal b on a.enquiry_id = b.enquiry_id inner join T_Motor_Enquiry_Response c on a.enquiry_id = c.enquiry_id";

            string fileds = "a.AgencyId as AgencyID, a.AgencyName as AgencyName,a.Enquiry_Id as Enquiry_Id, a.vehicletype as Insurance,";
            fileds = fileds + "(select top 1 CompanyName from tblMaster_ProductDetails where CompanyId = c.chainid ) as Insurer,";
            fileds = fileds + "c.policynumber as PolicyNo, (select JSON_VALUE(proposalresponse,'$.vehicle.vehicleIDV.idv') from T_Motor_Enquiry_Response where enquiry_id = a.enquiry_id) as SumInsured,";
            fileds = fileds + "CONCAT(b.title,' ',b.firstname,' ',b.lastname) as Proposer_Name, CONCAT(a.BrandName,' ',a.ModelName,' ',a.VarientName) as Insured_Name,";
            fileds = fileds + "(select JSON_VALUE(proposalresponse,'$.ProductDetails.netPremium') from T_Motor_Enquiry_Response where enquiry_id = a.enquiry_id) as Basic,";
            fileds = fileds + "(select JSON_VALUE(proposalresponse,'$.ProductDetails.totalTax') from T_Motor_Enquiry_Response where enquiry_id = a.enquiry_id) as GST,";
            fileds = fileds + "(select JSON_VALUE(proposalresponse,'$.ProductDetails.grossPremium') from T_Motor_Enquiry_Response where enquiry_id = a.enquiry_id) as Premium,";
            fileds = fileds + "0 as Discount,0 as TDS, (select JSON_VALUE(proposalresponse,'$.ProductName') from T_Motor_Enquiry_Response where enquiry_id = a.enquiry_id) as ProductName,";
            fileds = fileds + "b.policyholdertype as PolicyType, '---' as Tenure,b.createddate as PolicyDate";

            string whereCondition = "(c.policynumber is not null and c.policynumber <> '')";

            if (!string.IsNullOrEmpty(agencyId))
            {
                whereCondition = whereCondition + " and a.AgencyId='" + agencyId + "'";
            }

            if (string.IsNullOrEmpty(fromDate) && string.IsNullOrEmpty(toDate))
            {
                whereCondition = whereCondition + " and  c.CreatedDate > (select CAST(CAST(GETDATE() AS DATE) AS DATETIME))";
            }
            else
            {
                if (!string.IsNullOrEmpty(fromDate))
                {
                    whereCondition = whereCondition + " and  c.CreatedDate > '" + fromDate + "'";
                }

                if (!string.IsNullOrEmpty(toDate))
                {
                    whereCondition = whereCondition + " and  c.CreatedDate <= DATEADD(Day, 1, '" + toDate + "')";
                }
            }
            if (!string.IsNullOrEmpty(insuranceType) && insuranceType.ToLower() == "motor")
            {
                whereCondition = whereCondition + " and (a.vehicletype='Private_Car' or a.vehicletype='Bike')";
            }
            if (!string.IsNullOrEmpty(CustomerName) && insuranceType.ToLower() == "motor")
            {
                whereCondition = whereCondition + " and (CONCAT(b.title,' ',b.firstname,' ',b.lastname) like '%" + CustomerName + "%')";
            }
            if (!string.IsNullOrEmpty(insurer) && insuranceType.ToLower() == "motor")
            {
                whereCondition = whereCondition + " and ((select top 1 CompanyName from tblMaster_ProductDetails where CompanyId = c.chainid ) = '" + insurer + "')";
            }

            if (!string.IsNullOrEmpty(product) && insuranceType.ToLower() == "motor")
            {
                whereCondition = whereCondition + " and (select JSON_VALUE(proposalresponse,'$.ProductName') from T_Motor_Enquiry_Response where ISJSON(proposalresponse) = 1 and proposalresponse is not null and enquiry_id = a.enquiry_id) = '" + product + "')";
            }

            whereCondition = whereCondition + " order by c.CreatedDate desc";

            return Post_Database.GetRecordFromTable(fileds, tablename, whereCondition, "motor");
        }

        public static DataTable GetAgentDetail()
        {
            string fileds = "AgencyId, AgencyName";
            string tablename = "aspnet_user";
            string wherecon = "AgencyName is NOT NULL and IsApproved=1";
            return Post_Database.GetRecordFromTable(fileds, tablename, wherecon, "api");
        }
        public static DataTable GetCompanyDetail(string insuranceType)
        {
            string fileds = string.Empty;
            string tablename = string.Empty;
            string wherecon = string.Empty;
            string database = string.Empty;

            if (insuranceType.ToLower() == "health")
            {
                fileds = "CompanyId as CompanyId, CompanyName as CompanyName,logoUrl as ImgUrl";
                tablename = "tbl_Company";
                database = "api";
            }
            else
            {
                fileds = "CompanyId as CompanyId, Supplier as CompanyName, Image as ImgUrl";
                tablename = "T_MotorCredential";
                database = "motor";
            }
            return Post_Database.GetRecordFromTable(fileds, tablename, wherecon, database);
        }

        public static DataTable GetCompanyProductDetail(string insuranceType, string insuranceId)
        {
            string fileds = string.Empty;
            string tablename = string.Empty;
            string wherecon = string.Empty;
            string database = string.Empty;

            if (insuranceType.ToLower() == "health")
            {
                fileds = "ProductName as ProductName,ProductId as ProductId";
                tablename = "tbl_Product";
                wherecon = "CompanyId = " + insuranceId + " and Active = 1";
                database = "api";
            }
            else
            {
                fileds = "Distinct ProductName as ProductName";
                tablename = "tblMaster_ProductDetails";
                wherecon = "CompanyId = " + insuranceId;
                database = "motor";
            }
            return Post_Database.GetRecordFromTable(fileds, tablename, wherecon, database);
        }

        #region[Agency Passbook/Ledger]
        public static bool AgencyPassbookUpdate(Dictionary<string, string> dobj, string AgencyId, decimal trxAmt)
        {
            bool issuccess = false;
            try
            {
                string fileds = string.Empty;
                string fieldValue = string.Empty;

                foreach (var item in dobj)
                {
                    fileds = fileds + item.Key + ",";
                    fieldValue = fieldValue + "'" + item.Value + "'" + ",";
                }
                fileds = fileds.Remove(fileds.Length - 1);
                fieldValue = fieldValue.Remove(fieldValue.Length - 1);

                DataTable dt = Post_Database.GetRecordFromTable("top 1 Balance", "T_Passbook", "AgencyId ='" + AgencyId + "' order by id desc", "seeinsured");
                decimal balanceAmt = 0;
                if (dt!=null && dt.Rows.Count > 0)
                {
                    balanceAmt = Convert.ToDecimal(dt.Rows[0]["Balance"].ToString());
                }

                balanceAmt = balanceAmt + trxAmt;
                fileds = fileds + " , Balance";
                fieldValue = fieldValue + " ," + balanceAmt;

                issuccess = Post_Database.InsertRecordToTable(fileds, fieldValue, "T_Passbook", "seeinsured");
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return issuccess;
        }
        public static DataTable GetAgencyPassbookList(string AgencyId, string FromDate, string ToDate)
        {
            DataTable dt = new DataTable();
            try
            {
                string whereCondition = "Status = 1";
                if (!string.IsNullOrEmpty(AgencyId)) 
                {
                    whereCondition = whereCondition + " and AgencyId ='" + AgencyId + "'";
                }

                if (!string.IsNullOrEmpty(FromDate))
                {
                    FromDate = Convert.ToDateTime(FromDate).ToString("yyyy-MM-dd HH:mm:ss.fff");
                    whereCondition = whereCondition + " and updateddate >= '" + FromDate + "'";
                }

                if (!string.IsNullOrEmpty(ToDate))
                {
                    ToDate = Convert.ToDateTime(ToDate).AddDays(1).AddMilliseconds(-1).ToString("yyyy-MM-dd HH:mm:ss.fff");
                    whereCondition = whereCondition + " and updateddate <= '" + ToDate + "'";
                }

                dt = Post_Database.GetRecordFromTable("*", "T_Passbook", whereCondition, "seeinsured");
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return dt;
        }
        #endregion
    }
}
