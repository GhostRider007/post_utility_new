﻿using Post_Utility.Database;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Post_Utility.FrontSection
{
    public static class Administration_Helper
    {
        public static DataTable GetAgentDetailByUserId(string userid)
        {
            string fileds = "*";
            string tablename = "aspnet_user";
            string wherecon = "IsApproved=1 and UserName='" + userid + "'";
            return Post_Database.GetRecordFromTable(fileds, tablename, wherecon, "api");
        }

        public static void UpdateAgencyLastLogin(string userid)
        {
            Post_Database.UpdateRecordIntoAnyTable("aspnet_user", "AgencyLastLoginDate=getdate()", "UserName='" + userid + "'", "api");
        }
        public static DataTable GetAgentDetailByAgencyId(string agencyId)
        {
            string fileds = "*";
            string tablename = "aspnet_user";
            string wherecon = "AgencyId='" + agencyId + "'";
            return Post_Database.GetRecordFromTable(fileds, tablename, wherecon, "api");
        }
        public static bool Update_AgencyPassword(string password, string agencyId)
        {
            string fieldsWithValue = "Password='" + password + "',UpdatedDate=getdate()";
            string tableName = "aspnet_user";
            string whereCondition = "AgencyId='" + agencyId + "'";
            return Post_Database.UpdateRecordIntoAnyTable(tableName, fieldsWithValue, whereCondition, "api");
        }
        public static bool UpdateLastLoginDate(string userId)
        {
            string fieldsWithValue = "LastLoginDate=getdate()";
            string tableName = "T_Agency";
            string whereCondition = "UserId=" + userId;
            return Post_Database.UpdateRecordIntoAnyTable(tableName, fieldsWithValue, whereCondition, "seeinsured");
        }
        public static bool Insert_LeadRequest(string agencyName, string agencyId, string name, string email, string contactNo, string age, string gender, string service, string remark)
        {
            string fields = "AgencyName,AgencyID,Name,Email,ContactNo ,Age ,Gender ,Service ,Remark";
            string fieldValue = "'" + agencyName + "','" + agencyId + "','" + name + "','" + email + "','" + contactNo + "','" + age + "','" + gender + "','" + service + "','" + remark + "'";
            string tableName = "T_LeadRequest";
            return Post_Database.InsertRecordToTable(fields, fieldValue, tableName, "seeinsured");
        }
        public static DataTable GetEnquiryDetailsByAgencyPro(string agencyid)
        {
            string fileds = "a.Id,a.AgencyName, a.CreatedDate,a.enquiry_id,a.ModelName,a.VarientName,a.vehicletype,b.proposalnumber as ProposalNo,b.chainid as ChainId,b.suminsured as PremimumAmount,c.firstname as FirstName, c.lastname as lastName ,b.policynumber,b.policypdflink";
            string tablename = "T_NewEnquiry_Motor a inner Join T_Motor_Enquiry_Response b on a.enquiry_id = b.enquiry_id  inner Join T_Motor_Proposal c on a.enquiry_id = c.enquiry_id";
            string whereCondition = " a.AgencyId='" + agencyid + "' and b.transactionnumber is  null and b.proposalnumber is not Null and b.paymentstatus is null  order by createddate desc";

            return Post_Database.GetRecordFromTable(fileds, tablename, whereCondition, "motor");
        }
    }
}
